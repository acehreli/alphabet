/*
 * trileri kütüphanesi sıralama denemesi CGI programı
 *
 * Çalışmasını görmek için: http://ddili.org/cgi-bin/trileri_deneme
 */

import tr.alfabe;
import tr.dizgi;
import tr.harfler;
import tr.yazi_parcasi;
import std.stdio;
import std.process;
import std.algorithm;
import std.string;
import std.conv;
import std.random;

/*
 * Onaltılı karakterin değerini döndürür
 */
int onaltılıDeğer(char hane)
{
    if (hane >= '0' && hane <= '9') {
        return hane - '0';

    } else if (hane >= 'A' && hane <= 'F') {
        return 10 + hane - 'A';

    } else if (hane >= 'a' && hane <= 'f') {
        return 10 + hane - 'a';

    } else {
        assert(false, "Geçersiz onaltılı karakter: " ~ to!string(hane));
    }
}

unittest
{
    assert(onaltılıDeğer('0') == 0);
    assert(onaltılıDeğer('9') == 9);
    assert(onaltılıDeğer('A') == 10);
    assert(onaltılıDeğer('F') == 15);
}

/*
 * HTML sorgusundaki karakterleri dönüştürür
 *
 *   % karakterini izleyen iki hane, onaltılı değer olarak
 *   + karakteri boşluk olarak
 */
string sorguDönüştür(string giriş)
{
    ubyte[] sonuç;

    int i;

    while (i < giriş.length) {
        if (giriş[i] == '%') {
            ++i;
            int karakterKodu =
                onaltılıDeğer(giriş[i]) * 16 + onaltılıDeğer(giriş[i + 1]);
            sonuç ~= cast(ubyte)karakterKodu;
            i += 2;

        } else {
            sonuç ~= (giriş[i] == '+' ? ' ' : giriş[i]);
            ++i;
        }
    }

    return cast(string)sonuç;
}

unittest
{
    assert(sorguDönüştür("merhaba+ğ+selam%21") == "merhaba ğ selam!");
}

/*
 * sorgu içinde bulunan ve "isim=veri&" düzeninde gelen CGI sorgu
 * değerlerini ayrıştırır
 */
string[string] ayrıştır(string sorgu)
{
    string[string] sonuç;

    foreach (veri; split(sorgu, "&")) {
        auto isim_değer = split(veri, "=");
        sonuç[isim_değer[0]] = isim_değer[1];
    }

    return sonuç;
}

unittest
{
    auto sonuç = ayrıştır("i0=v0&i1=v1&i2=v2");
    assert("i0" in sonuç);
    assert("i1" in sonuç);
    assert("i2" in sonuç);
    assert(sonuç["i0"] == "v0");
    assert(sonuç["i1"] == "v1");
    assert(sonuç["i2"] == "v2");
}

/*
 * trileri kütüphanesinin hünerleri
 */
struct Hünerler
{
    string isim;
    string harfler;
    string[] sıralılar;
    string[] küçükler;
    string[] büyükler;
    string[] aksansızlar;
}

/*
 * Tek bir string halinde gelen satırları alfabe koduna göre sıralar
 */
Hünerler hünerler(string kod)(string birleşikSatırlar)
{
    Hünerler sonuç;

    string[] satırlar = splitLines(birleşikSatırlar);
    Dizgi!kod[] alfabeSatırları;

    /*
     * Ham olarak gelen satırlardan alfabe dizgileri oluşturuyoruz
     */
    foreach (satır; satırlar) {
        alfabeSatırları ~= Dizgi!kod(dtext(satır));
    }

    // Sıralanmaları çok kolay:
    alfabeSatırları.sort();

    sonuç.isim = alfabe!kod.isim();
    sonuç.harfler = alfabe!kod.toString();

    foreach (satır; alfabeSatırları) {
        sonuç.sıralılar ~= satır.toString();
        sonuç.küçükler ~= satır.küçüğü.toString();
        sonuç.büyükler ~= satır.büyüğü.toString();
        sonuç.aksansızlar ~= satır.aksansızı.toString();
    }

    return sonuç;
}

/*
 * Evet, bu işlevi temizlemek gerek! :)
 */
Hünerler yanıtla(string seçiliAlfabe, string liste)
{
    if (seçiliAlfabe == alfabe!"aze".isim()) {
        return hünerler!"aze"(liste);

    } else if (seçiliAlfabe == alfabe!"gag".isim()) {
        return hünerler!"gag"(liste);

    } else if (seçiliAlfabe == alfabe!"kjh".isim()) {
        return hünerler!"kjh"(liste);

    } else if (seçiliAlfabe == alfabe!"eng".isim()) {
        return hünerler!"eng"(liste);

    } else if (seçiliAlfabe == alfabe!"gle".isim()) {
        return hünerler!"gle"(liste);

    } else if (seçiliAlfabe == alfabe!"crh".isim()) {
        return hünerler!"crh"(liste);

    } else if (seçiliAlfabe == alfabe!"kum".isim()) {
        return hünerler!"kum"(liste);

    } else if (seçiliAlfabe == alfabe!"kur".isim()) {
        return hünerler!"kur"(liste);

    } else if (seçiliAlfabe == alfabe!"tur".isim()) {
        return hünerler!"tur"(liste);

    } else if (seçiliAlfabe == alfabe!"tuk".isim()) {
        return hünerler!"tuk"(liste);

    } else {
        throw new Exception("Geçersiz alfabe");
    }
}

/*
 * Tek bir alfabe seçeneği satırı
 */
string alfabeSeçeneği(string kod)(string seçiliAlfabe)
{
    string isim = alfabe!kod.isim();

    return
        "\n"
        ~ ` <input type="RADIO"`
        ~ (isim == seçiliAlfabe ? " checked" : "")
        ~ ` name="alfabe" value="` ~ isim ~ `">`
        ~ ` <b>` ~ isim ~ ":</b> " ~ alfabe!kod.toString() ~ "<br>";
}

/*
 * Bütün alfabe seçenekleri
 */
string alfabeSeçenekleri(string seçiliAlfabe)
{
    return
        alfabeSeçeneği!"aze"(seçiliAlfabe) ~
        alfabeSeçeneği!"gag"(seçiliAlfabe) ~
        alfabeSeçeneği!"kjh"(seçiliAlfabe) ~
        alfabeSeçeneği!"eng"(seçiliAlfabe) ~
        alfabeSeçeneği!"gle"(seçiliAlfabe) ~
        alfabeSeçeneği!"crh"(seçiliAlfabe) ~
        alfabeSeçeneği!"kum"(seçiliAlfabe) ~
        alfabeSeçeneği!"kur"(seçiliAlfabe) ~
        alfabeSeçeneği!"tur"(seçiliAlfabe) ~
        alfabeSeçeneği!"tuk"(seçiliAlfabe);
}

immutable rasgeleSatırDüğmesi = "Rasgele Satırlar";

Dizgi!kod rasgeleDizgi(string kod)(int uzunluk)
{
    string alfabe = alfabe!kod.toString();
    dstring harfler = dtext(alfabe);

    /*
     * Alfabe harflerinin şanslarını arttırmak için başındaki rakamları
     * atlayarak tekrarlıyoruz
     */
    foreach (harfTekrarı; 0 .. 2) {
        harfler ~= dtext(alfabe)[22 .. $];
    }

    // Boşluk karakterinin şansını arttırmak için
    harfler ~= "                ";

    // Aksanlar
    harfler ~= a_aksan.grup;
    harfler ~= A_aksan.grup;
    harfler ~= i_aksan.grup;
    harfler ~= I_aksan.grup;
    // XXX - Başka aksanlar da gerekiyor

    auto sözcük = Dizgi!kod();
    foreach (i; 0 .. uzunluk) {
        sözcük ~= harfler[uniform(0, harfler.length)];
    }
    return sözcük;
}

Dizgi!kod birazDeğişiği(string kod)(const Dizgi!kod sözcük)
{
    auto sonuç = Dizgi!kod();

    foreach (harf; sözcük) {
        final switch (uniform(0, 3)) {
        case 0:
            sonuç ~= harf.küçüğü;
            break;

        case 1:
            sonuç ~= harf.büyüğü;
            break;

        case 2:
            sonuç ~= harf.aksansızı;
            break;
        }
    }

    return sonuç;
}

YazıParçası[] rasgeleDizgiler(string kod)(int grupAdedi)
{
    YazıParçası[] sonuç;

    foreach (sonuçSayacı; 0 .. grupAdedi) {
        Dizgi!kod temel = rasgeleDizgi!kod(uniform(2, 5));

        foreach (benzerSayacı; 0 .. uniform(5, 10)) {
            auto dizgi = birazDeğişiği(temel) ~ rasgeleDizgi!kod(uniform(3, 5));
            sonuç ~= dizgi_yazı_parçası(dizgi);
        }
    }
    return sonuç;
}

/*
 * Evet, bu korkunçluğu da temizleyeceğim. :)
 */
string rasgeleSatırDoldur(string seçiliAlfabe, int grupAdedi)
{
    YazıParçası[] satırlar;

    if (seçiliAlfabe == alfabe!"aze".isim()) {
        satırlar = rasgeleDizgiler!"aze"(grupAdedi);

    } else if (seçiliAlfabe == alfabe!"gag".isim()) {
        satırlar = rasgeleDizgiler!"gag"(grupAdedi);

    } else if (seçiliAlfabe == alfabe!"kjh".isim()) {
        satırlar = rasgeleDizgiler!"kjh"(grupAdedi);

    } else if (seçiliAlfabe == alfabe!"eng".isim()) {
        satırlar = rasgeleDizgiler!"eng"(grupAdedi);

    } else if (seçiliAlfabe == alfabe!"gle".isim()) {
        satırlar = rasgeleDizgiler!"gle"(grupAdedi);

    } else if (seçiliAlfabe == alfabe!"crh".isim()) {
        satırlar = rasgeleDizgiler!"crh"(grupAdedi);

    } else if (seçiliAlfabe == alfabe!"kum".isim()) {
        satırlar = rasgeleDizgiler!"kum"(grupAdedi);

    } else if (seçiliAlfabe == alfabe!"kur".isim()) {
        satırlar = rasgeleDizgiler!"kur"(grupAdedi);

    } else if (seçiliAlfabe == alfabe!"tur".isim()) {
        satırlar = rasgeleDizgiler!"tur"(grupAdedi);

    } else if (seçiliAlfabe == alfabe!"tuk".isim()) {
        satırlar = rasgeleDizgiler!"tuk"(grupAdedi);

    } else {
        throw new Exception("Geçersiz alfabe");
    }

    string sonuç;
    foreach (satır; satırlar) {
        sonuç ~= satır.toString();
        sonuç ~= '\n';
    }
    return sonuç;
}

string küçükBüyükAksansız(const ref Hünerler hünerler)
in
{
    assert(hünerler.küçükler.length == hünerler.büyükler.length);
    assert(hünerler.küçükler.length == hünerler.aksansızlar.length);
}
body
{
    string sonuç = "<b>Küçük / Büyük / Aksansız "
                   "(Lowercase / Uppercase / Unaccented)</b><br>";

    foreach (i, satır; hünerler.küçükler) {
        string girinti = (i % 2) ? "&nbsp;" : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        sonuç ~= format("%s%s<br>%s%s<br>%s%s<br>\n",
                        girinti, hünerler.küçükler[i],
                        girinti, hünerler.büyükler[i],
                        girinti, hünerler.aksansızlar[i]);
    }

    return sonuç;
}

version(unittest) 
{

void main()
{
}

} else { // version(unittest)

void main()
{
    string sorgu = sorguDönüştür(getenv("QUERY_STRING"));
    string[string] değerler = ayrıştır(sorgu);

    if (("düğme" in değerler)
        &&
        (değerler["düğme"] == rasgeleSatırDüğmesi)) {

        foreach (i; 0 .. 2 ) {
            try {
                değerler["alan"] = rasgeleSatırDoldur(değerler["alfabe"], 10);
                break; // Başardık

            } catch (Throwable hata) {
                // Varsayılan alfabe Türkçe
                değerler["alfabe"] = alfabe!"tur".isim();
            }
        }
    }

    Hünerler hünerler;

    try {
        hünerler = yanıtla(değerler["alfabe"], değerler["alan"]);

    } catch (Throwable hata) {
        // Varsayılan alfabe Türkçe
        değerler["alfabe"] = alfabe!"tur".isim();
    }

    if (!hünerler.sıralılar.length) {
        // Liste boş başlamasın diye
        hünerler.sıralılar = [ "ek",
                               "aÂbCÇdefgğhIÎİjklmnoÖpqrsştuÛüvwxyz",
                               "hÂl",
                               "ak",
                               "hala",
                               "aâbcçdefgğhıîijklmnoöpqrsştuûüvwxyz",
                               "hâl",
                               "hal"
                               ];
    }

    /*
     * Evet, gerçekten bir web sunucusu kütüphanesine ihtiyaç var! :)
     */

    write("Content-type: text/html; charset=utf-8\n\n");
    write("<html>\n  <body>\n");

    write(
        `<h2>trileri kütüphanesi deneme programı</h2>`
        `<p>See the explanations at the bottom of the page. | `
        `Açıklaması sayfanın sonunda.</p>`
        `<form method="GET" action="http://ddili.org/cgi-bin/trileri_deneme">`

        ~ alfabeSeçenekleri(değerler["alfabe"]) ~

        `<br>`
        `<b>Sıralanacak satırları yazın, veya </b>`
        `<input type="Submit" value="` ~ rasgeleSatırDüğmesi ~
        `" name="düğme">`

        `<br>`
        `<textarea name="alan" rows="16" cols="60">`
        ~ join(hünerler.sıralılar, "\n") ~
        `</textarea>`

        `<br>
        <input type="Submit" value="Sırala" name="düğme">`
        `</form>`

        `<hr>`

        ~ küçükBüyükAksansız(hünerler)

        ~ `<hr>`

        ~ açıklama

        ~ explanation
                );

    write("\n<hr><br>\n<a href=\""
          "http://ddili.org/ornek_kod/trileri_deneme.d\">"
          "(Bu çıktıyı oluşturan program)</a><br>\n");

    write("\n  </body>\n</html>\n");
}

} // version(unittest)

static string açıklama =
    `<h2>trileri kütüphanesi</h2>`

    `<p>trileri, <a href="http://ddili.org/">Ddili Forum</a> üyelerinin `
    `<a href="http://ddili.org/ders/d/">D programlama dili</a> ile `
    `geliştirdikleri bir kütüphanedir. Bu kütüphane, D'nin Unicode `
    `yeteneklerini kullanarak bir <i>alfabe</i> kavramı kurar ve kendi `
    `alfabesine uygun olarak küçültülen, büyütülen, ve sıralanan Dizgi `
    `şablonunu tanımlar. trileri'nin kaynak kodlarını `
    `<a href="http://code.google.com/p/trileri/source/checkout">proje `
    `sayfasından</a> edinebilirsiniz.</p>`

    `<p>Alfabe harfleriyle ilgili olduğu için, dizgileri karakter kodlarına `
    `göre değil, alfabe kurallarına göre sıralar:</p>`

    `<ul>`
    `<li>Sıralamada öncelikle temel harfe bakılır; farklı olan ilk harfi `
    `alfabede önce olan öncedir: örneğin "ak", "ek"ten öncedir</li>`
    `<li>Eğer iki dizginin bütün harfleri, aksanlarına bakılmadan temelde `
    `aynıysa, kısa olan dizgi öncedir: örneğin "hâl", "hala"dan öncedir</li>`
    `<li>Eğer iki dizginin uzunlukları da aynıysa, aksanlı olarak farklı olan `
    `ilk harfe bakılır; aksansız olan öncedir: örneğin "hal", "hâl"den `
    `öncedir</li>`
    `<li>Eğer aksanlı harfler konusunda da aynı iseler, küçük/büyük olarak `
    `farklı olan ilk harfe bakılır; küçük olan öncedir: örneğin "hâl", `
    `"hÂl"den öncedir</li>`
    `</ul>`

    `<p>Hem bu sıralama kurallarını, hem de harflerin küçük, büyük, ve `
    `aksansız dönüşümlerini bu sayfadaki kutuya yazacağınız satırlarla `
    `deneyebilirsiniz. Sonuçlar seçtiğiniz alfabeye göre farklı olacaktır.</p>`

    `<p>Rastladığınız yanlışlıkları veya önerilerinizi lütfen `
    `<a href="http://ddili.org/forum/forum/6">trileri forumunda</a> `
    `bildirin.</p>`

    `<p>Teşekkürler! ☺</p>`
    ;

static string explanation =
    `<h2>The trileri Library</h2>`

    `<p>trileri is a library that has been developed by the members of the `
    `<a href="http://ddili.org/">Ddili Forum</a>. trileri is written in the`
    `<a href="http://ddili.org/ders/d.en/">programming language</a>. This library defines the concept of an <i>alphabet</i> taking advantage of D's Unicode support. tr.Dizgi is a string class, templatized by alphabet code, providing lowercasing, uppercasing, and letter comparisons according to that alphabet. trileri project is at `
    `<a href="http://code.google.com/p/trileri">http://code.google.com/p/trileri<a/> `
    `</p>`

    `<p>Because it knows about alphabets, trileri sorts strings according to the sort order of each alphabet, not by character codes:</p>`

    `<ul>`
    `<li>Order is determined by base, unaccented letters first: First different letters are compared. For example "as" is before "at"</li>`

    `<li>If all unaccented forms of all of the letters are equal, the shorter string comes first: For example "plié" is before "pliers"</li>`

    `<li>If the lengths of the two strings are equal, then the first letter that differ in accentedness determine the order; the unaccented letter comes first: For example "resume" is before "résumé"</li>`

    `<li>If two strings are equal even down to accents, then lowercase vs. uppercase determine the ordering; lowercase comes first: For example "élite" is before "ÉLITE"</li>`
    `</ul>`

    `<p>You can test the library by entering lines in the box below. The ordering would be different per alphabet.</p>`

    `<p>Please report bugs and suggestions to Ali Çehreli at acehreli@yahoo.com.</p>`

    `<p>Thank you! ☺</p>`
    ;
