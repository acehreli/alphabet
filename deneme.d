/**
 * Copyright: Açık kod
 * Version: 0.1
 *
 */

module tr.deneme;

version(unittest) 
{

void main()
{
}

} else { // version(unittest)

import tr.string;
import tr.uni;
import std.datetime;
import std.stdio;

void main(string[] argümanlar)
{
    if ((argümanlar.length == 2) && (argümanlar[1] == "hiz")) {
        hızTesti();
    }
}

void hızTesti()
{
    writeln("toUpper_tr: ", benchmark!(birinci_yöntem)(1_000_000));
    writeln("capitalize_tr: ", benchmark!(ikinciYöntem)(1_000_000));

    /* std.date den dahil ettiğimiz benchmark şablonuyla fonksiyonun
     * yürütülme süresini ölçüyoruz. Şablon parametresi, hangi fonksiyonu
     * yürüteceği, fonksiyon parametresi ise kaç kere yürüteceğidir.
     */
}

dstring denemeDizgisi =
    "denemebenidenerimsenidenemebenidenerimsenidenemebenidenerimseni"d
    "denemebenidenerimsenidenemebenidenerimsenidenemebenidenerimseni"d
    "denemebenidenerimsenidenemebenidenerimsenidenemebenidenerimseni"d
    "denemebenidenerimseni"d;

void birinci_yöntem()
{
    toUpper_tr(denemeDizgisi);
}

void ikinciYöntem()
{
    capitalize_tr(denemeDizgisi);
}

} // version(unittest)