/**
 * Bu modül, YazıParçası nesnelerinin bir araya gelmelerinden oluşan Yazı
 * sınıfını tanımlar.
 */
module tr.yazi;

import tr.yazi_parcasi;
import tr.dizgi;

import std.conv;
import std.algorithm;

debug(tr_yazi) import std.stdio;


/**
 * Yazı parçalarından oluşan yazı kavramını temsil eder
 *
 * Her parça farklı bir alfabeye bağlı olabilir. Bütün yazının küçültülmesi,
 * büyütülmesi, ve aksansızına çevrilmesi her parçanın kendi kurallarına
 * göre gerçekleştirilir.
 *
 * İki yazının sıra karşılaştırması ise bütün yazı için yeğlenen alfabeye
 * göre yapılır.
 */
class Yazı(string yeğlenenAlfabe)
{
private:

    YazıParçası[] parçalar_;

public:

    this()
    {
        parçalar_ = parçalar_.init;
    }

    this(YazıParçası parça)
    {
        parçalar_ ~= parça;
    }

    override bool opEquals(Object o) const
    {
        auto sağdaki = cast(Yazı)o;
        if (sağdaki is null) {
            return false;
        }

        dchar[] hepsi;
        foreach(parça; sağdaki.parçalar_) {
            hepsi ~= parça.karakterler();
        }

        return this == hepsi;
    }

    bool opEquals(const dchar[] dizgi) const
    {
        if (length != dizgi.length) {
            return false;
        }

        /*
         * Aşağıdaki algoritma uzunlukların eşit olduğunu varsayar; hemen
         * yukarıda işlevden çıkmış olsak bile, daha ileri gitmeden bunu
         * denetleyelim
         */
        assert(length == dizgi.length);

        int baş = 0;

        foreach (parça; parçalar_) {
            if (parça.length) {
                if (parça.karakterler[] != dizgi[baş .. baş + parça.length]) {
                    return false;
                }

                baş += parça.length;
            }
        }

        // Sonuna kadar fark bulamadık
        return true;
    }

    Yazı dup() const
    {
        Yazı yazı = new Yazı;

        foreach (parça; parçalar_) {
            yazı ~= parça.dup();
        }

        return yazı;
    }

    Yazı opOpAssign(string işleç)(YazıParçası parça)
        if (işleç == "~")
    {
        parçalar_ ~= parça;
        return this;
    }

    @property int length() const
    {
        int uzunluk;

        foreach (parça; parçalar_) {
            uzunluk += parça.length;
        }

        return uzunluk;
    }

    override string toString() const
    {
        string sonuç;

        foreach (parça; parçalar_) {
            sonuç ~= parça.toString();
        }

        return sonuç;
    }

    /**
     * Yazı parçalarına erişim sağlayan foreach
     */
    int opApply(int delegate(ref YazıParçası) işlem)
    {
        int sonuç = 0;

        foreach (ref parça; parçalar_) {
            sonuç = işlem(parça);
            if (sonuç) {
                break;
            }
        }

        return sonuç;
    }

    /**
     * Yazı parçalarına sayaçlı erişim sağlayan foreach
     */
    int opApply(int delegate(ref int i, ref YazıParçası) işlem)
    {
        int sonuç = 0;
        int sayaç = 0;

        foreach (ref parça; parçalar_) {
            sonuç = işlem(sayaç, parça);
            if (sonuç) {
                break;
            }
            ++sayaç;
        }

        return sonuç;
    }

    @property Yazı küçüğü() const
    {
        auto sonuç = new Yazı!yeğlenenAlfabe;

        foreach (parça; parçalar_) {
            sonuç.parçalar_ ~= parça.küçüğü;
        }

        return sonuç;
    }

    @property Yazı büyüğü() const
    {
        auto sonuç = new Yazı!yeğlenenAlfabe;

        foreach (parça; parçalar_) {
            sonuç.parçalar_ ~= parça.büyüğü;
        }

        return sonuç;
    }

    @property Yazı aksansızı() const
    {
        auto sonuç = new Yazı!yeğlenenAlfabe;

        foreach (parça; parçalar_) {
            sonuç.parçalar_ ~= parça.aksansızı;
        }

        return sonuç;
    }
}

unittest
{
    import tr.dizgi;

    alias Yazı!"tur" yazı_tr;
    alias Dizgi!"tur" dizgi_tr;
    alias Dizgi!"eng" dizgi_en;

    auto yazı = new yazı_tr;
    assert(yazı.length == 0);

    YazıParçası[] parçalar = [
        cast(YazıParçası)dizgi_yazı_parçası(dizgi_tr("Ali")),
        cast(YazıParçası)dizgi_yazı_parçası(dizgi_tr(" ")),
        cast(YazıParçası)dizgi_yazı_parçası(dizgi_en("Jim"))
    ];

    foreach (parça; parçalar) {
        yazı ~= parça;
    }

    auto yazı2 = yazı.dup;
    assert(yazı2 == yazı);
    yazı2 ~= dizgi_yazı_parçası(dizgi_tr("a"));
    assert(yazı != yazı2);

    assert(yazı.length == 7);
    assert(yazı == "Ali Jim");

    assert(yazı != "Ali JiM");

    assert(yazı.büyüğü == "ALİ JIM");
    assert(yazı.küçüğü == "ali jim");

    assert((new yazı_tr(dizgi_yazı_parçası(dizgi_tr("aâçiîuûü")))).aksansızı
           == "aaçiıuuü");
    assert((new yazı_tr(dizgi_yazı_parçası(dizgi_en("aâçiîuûü")))).aksansızı
           == "aaciiuuu");

    // Sayaçsız, değiştirmeyen foreach
    int testSayacı = 0;
    foreach (parça; yazı) {
        assert(parça.eşittir(parçalar[testSayacı]));
        ++testSayacı;
    }

    // Sayaçsız, değiştiren foreach
    testSayacı = 0;
    foreach (ref parça; yazı) {
        parça = parçalar[testSayacı].büyüğü;
        parçalar[testSayacı] = parçalar[testSayacı].büyüğü;
        ++testSayacı;
    }
    assert(yazı == "ALİ JIM");

    // Sayaçlı, değiştirmeyen foreach
    testSayacı = 0;
    foreach (i, parça; yazı) {
        assert(i == testSayacı);
        assert(parça.toString() == parçalar[testSayacı].toString());
        ++testSayacı;
    }

    // Sayaçlı, değiştiren foreach
    testSayacı = 0;
    foreach (i, ref parça; yazı) {
        assert(i == testSayacı);
        parça = dizgi_yazı_parçası(dizgi_tr(to!dstring(i)));
        ++testSayacı;
    }
    assert(yazı == "012");
}
