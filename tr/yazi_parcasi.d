/**
 * Bu modül, YazıParçası arayüzünü, ve onların bir araya gelmelerinden
 * oluşan Yazı sınıfını tanımlar.
 */

module tr.yazi_parcasi;

/**
 * Bir yazı içinde kullanılabilecek yazı parçası kavramını temsil eder
 */
interface YazıParçası
{
    bool eşittir(YazıParçası sağdaki) const;
    YazıParçası dup() const;

    @property size_t length() const;
    @property YazıParçası küçüğü() const;
    @property YazıParçası büyüğü() const;
    @property YazıParçası aksansızı() const;

    @property const(dchar)[] karakterler() const;

    string toString() const;
}
