/**
 * Bu modül, alfabe kavramını temsil eden Alfabe yapısını ve onunla ilgili
 * yardımcı yapıları içerir
 */

module tr.alfabe;

import tr.harf;
import tr.grup;
import tr.alfabe_harfi;
import tr.harfler;
import tr.im;

import std.uni;
import std.conv;
import std.exception;
import std.string;

debug(tr_alfabe)
{
    import std.stdio;
}

/**
 * Harflerin sıra bilgisini opCmp'un dönüş değerindeki gibi bildirir
 *
 *     $(UL
 *     $(LI == 0  aynı sıradalar)
 *     $(LI  < 0  soldaki önce)
 *     $(LI  > 0  soldaki sonra)
 *     )
 */
struct Sıra
{
    /**
     * Harflerin temelde karşılaştırmalarını ifade eder
     *
     * Bu değer, 'a' ile 'b'nin farklı olduğunu gösteren değerdir.
     * Küçük/Büyük ayrımı yapmaz: 'C' ile 'c' bu karşılaştırmada eşit kabul
     * edilir
     */
    int temel;

    /**
     * Harflerin aksanları göz önüne alınarak karşılaştırılmalarını ifade
     * eder
     *
     * Bu değer 'a'nın 'â'dan farklı olduğunu gösteren değerdir. temel'de
     * olduğu gibi, küçük/büyük ayrımı yapmaz.
     */
    int aksan;

    /**
     * Harflerin küçük/büyük anlamda karşılaştırılmalarını ifade eder
     *
     * Küçük harf, büyükten önce kabul edilir
     */
    int küçükBüyük;

    this(int temel, int aksan, int küçükBüyük)
    {
        this.temel = temel;
        this.aksan = aksan;
        this.küçükBüyük = küçükBüyük;
    }

    string toString()
    {
        return format("%s,%s,%s", temel, aksan, küçükBüyük);
    }
}

/**
 * Harfin küçük veya büyük olduğu
 *
 * Bu değerler sıralama amacıyla kullanıldıkları için küçük harfin büyükten
 * önce kabul edilmesi için küçük'ün daha küçük bir değeri olması gerekir
 */
enum KüçükBüyük { küçük = 0, büyük = 1 }

/* Yukarıdaki açıklamadaki koşulun denetlenmesi için */
static assert(KüçükBüyük.küçük < KüçükBüyük.büyük);

/**
 * Alfabe kurulurken otomatik olarak edinilen bilgileri içerir
 */
struct AlfabeHarfiBilgisi
{
    /*
     * Bu bilginin ait olduğu harf
     *
     * Not: Bunun bir gösterge olması, çalışma zamanında bunun üzerinden
     *      bir atlama (indirection) gerektireceği için az da olsa bir
     *      yavaşlık getirir.  bunun yerine AlfabeHarfi'ni buraya getirmeyi
     *      düşünebiliriz.
     */
    const(AlfabeHarfi) * harf;

    /*
     * Bu harfin aksansız olanı.
     **
     * Örneğin 'ç'ninki Türk alfabesinde 'ç'dir, İngiliz alfabesinde
     * 'c'dir. 'Â'nınki her iki dilde de 'A'dır.
     */
    Harf aksansızOlanı;

    /*
     * Bu harfin temel sıra numarası. Temel harf ve aksanlıları aynı sıra
     * numarasına sahiptirler
     */
    int temelSıraNumarası;

    /*
     * Bu harfin aksan sıra numarası
     *
     * Temel harflerde 0, aksanlılarda 0'dan büyük bir değer olur. Harfin
     * temel veya aksanlı olup olmadığı da bu değişkenden anlaşılır
     */
    int aksanSıraNumarası;

    /*
     * Bu harfin küçük/büyük sıra numarası
     *
     * Temelde ve aksanlılarda aynı olan harfler, bu değerde farklı
     * olabilirler. Örneğin 'z', 'Z'den önce sıralanır
     */
    int küçükBüyükSıraNumarası;

    this(const(AlfabeHarfi) * harf,
         Harf aksansızOlanı,
         int temelSıraNumarası,
         int aksanSıraNumarası,
         KüçükBüyük küçükBüyük)
    in
    {
        assert(temelSıraNumarası >= 0);
        assert(aksanSıraNumarası >= 0);
    }
    body
    {
        this.harf = harf;
        this.aksansızOlanı = aksansızOlanı;
        this.temelSıraNumarası = temelSıraNumarası;
        this.aksanSıraNumarası = aksanSıraNumarası;
        this.küçükBüyükSıraNumarası = küçükBüyük;
    }

    /*
     * Harfin aksanlı olup olmadığını bildirir
     */
    bool aksanlı_mı() const
    {
        return aksanSıraNumarası != 0;
    }

    /*
     * Harfin küçüğünü döndürür
     */
    @property dchar küçüğü() const
    {
        return (aksanlı_mı()
                ? harf.küçük_aksanlılar.grup[aksanSıraNumarası - 1]
                : harf.küçük.kod);
    }

    /*
     * Harfin büyüğünü döndürür
     */
    @property dchar büyüğü() const
    {
        return (aksanlı_mı()
                ? harf.büyük_aksanlılar.grup[aksanSıraNumarası - 1]
                : harf.büyük.kod);
    }

    /*
     * Harfin aksansız olanını döndürür
     *
     * Hangi harfin aksanlı kabul edildiği alfabeye bağlıdır
     */
    @property dchar aksansızı() const
    {
        return aksansızOlanı.kod;
    }

    /*
     * Harflerin sıra ilişkilerini döndürür
     */
    Sıra sıra(const ref AlfabeHarfiBilgisi sağdaki) const
    {
        return Sıra(temelSıraNumarası - sağdaki.temelSıraNumarası,
                    aksanSıraNumarası - sağdaki.aksanSıraNumarası,
                    küçükBüyükSıraNumarası - sağdaki.küçükBüyükSıraNumarası);
    }

    string toString()
    {
        return format("%s%s %s+%s", küçüğü, büyüğü,
                      temelSıraNumarası, aksanSıraNumarası);
    }
}

unittest
{
    immutable x_abc = AlfabeHarfi(Harf('x'), Harf('X'),
                                  Grup("abc"), Grup("ABC"));
    immutable x =
        AlfabeHarfiBilgisi(&x_abc, x_abc.küçük, 0, 0, KüçükBüyük.küçük);
    immutable b =
        AlfabeHarfiBilgisi(&x_abc, x_abc.küçük, 0, 2, KüçükBüyük.küçük);
    immutable B =
        AlfabeHarfiBilgisi(&x_abc, x_abc.büyük, 0, 2, KüçükBüyük.büyük);
    immutable c =
        AlfabeHarfiBilgisi(&x_abc, x_abc.küçük, 0, 3, KüçükBüyük.küçük);

    assert(!x.aksanlı_mı());
    assert(x.küçüğü == 'x');
    assert(x.büyüğü == 'X');
    assert(x.aksansızı == 'x');

    assert(b.aksanlı_mı());
    assert(b.küçüğü == 'b');
    assert(b.büyüğü == 'B');
    assert(x.aksansızı == 'x');

    Sıra xb = x.sıra(b);
    assert(xb.temel == 0);
    assert(xb.aksan < 0);

    Sıra cx = c.sıra(x);
    assert(cx.temel == 0);
    assert(cx.aksan > 0);

    Sıra bc = b.sıra(c);
    assert(bc.temel == 0);
    assert(bc.aksan < 0);

    Sıra cc = c.sıra(c);
    assert(cc.temel == 0);
    assert(cc.aksan == 0);
    assert(cc.küçükBüyük == 0);

    Sıra bB = b.sıra(B);
    assert(bB.temel == 0);
    assert(bB.aksan == 0);
    assert(bB.küçükBüyük < 0);

    immutable y_def = AlfabeHarfi(Harf('y'), Harf('Y'),
                                  Grup("def"), Grup("DEF"));
    immutable y =
        AlfabeHarfiBilgisi(&y_def, y_def.küçük, 1, 0, KüçükBüyük.küçük);
    immutable d =
        AlfabeHarfiBilgisi(&y_def, y_def.küçük, 1, 1, KüçükBüyük.küçük);
    immutable D =
        AlfabeHarfiBilgisi(&y_def, y_def.büyük, 1, 1, KüçükBüyük.büyük);

    Sıra xy = x.sıra(y);
    assert(xy.temel < 0);

    Sıra cd = c.sıra(d);
    assert(cd.temel < 0);

    Sıra dD = d.sıra(D);
    assert(dD.küçükBüyük < 0);

    /*
     * Küçüğü veya büyüğü bulunmayan harflerin kendileri
     * kullanılmalı. Örneğin İngiliz alfabesinde ı'nın büyüğü, İ'nin de
     * küçüğü yok
     */
    immutable farksızlı = AlfabeHarfi(Harf('j'),
                                      Harf('J'),
                                      Grup("Şğabcxyz"),
                                      Grup("ŞğABCXYZ"));

    immutable bilgi_ğ = AlfabeHarfiBilgisi(
        &farksızlı, farksızlı.küçük, 0, 2, KüçükBüyük.küçük);

    immutable bilgi_Ş = AlfabeHarfiBilgisi(
        &farksızlı, farksızlı.büyük, 0, 1, KüçükBüyük.büyük);

    assert(bilgi_ğ.küçüğü == 'ğ');
    // büyüğü bulunmadığı için kendisi olmalı
    assert(bilgi_ğ.büyüğü == 'ğ');
    assert(bilgi_ğ.aksansızı == 'j');

    // küçüğü bulunmadığı için kendisi olmalı
    assert(bilgi_Ş.küçüğü == 'Ş');
    assert(bilgi_Ş.büyüğü == 'Ş');
    assert(bilgi_Ş.aksansızı == 'J');
}

/**
 * Alfabe kavramını temsil eder
 *
 * Harflerin küçük/büyük hallerini, sıra ilişkilerini, aksanlılarını,
 * vs. bilir
 */
struct Alfabe
{
    // Alfabenin okunaklı ismi
    string isim_;

    // Alfabe kurulurken verilen harfleri içerir; alfabenin "ham" halidir
    AlfabeHarfi[] harfler_;

    // Harflerden toplanan bilgileri içerir
    AlfabeHarfiBilgisi[dchar] harfBilgileri_;

    this(string isim,
         AlfabeHarfi[] harfler,
         AlfabeHarfiBilgisi[dchar] harfBilgileri)
    {
        isim_ = isim;
        harfler_ = harfler;
        harfBilgileri_ = harfBilgileri;
    }

    /*
     * Harfin küçüğünü döndürür
     *
     * Eğer alfabede bulunan bir harf ise alfabe kurallarını, değilse
     * toLower'ı kullanır
     */
    dchar küçüğü(dchar harf) const
    {
        const AlfabeHarfiBilgisi * bilgi = harf in harfBilgileri_;
        return bilgi ? bilgi.küçüğü() : toLower(harf);
    }

    /*
     * Harfin büyüğünü döndürür
     *
     * Eğer alfabede bulunan bir harf ise alfabe kurallarını, değilse
     * toUpper'ı kullanır
     */
    dchar büyüğü(dchar harf) const
    {
        const AlfabeHarfiBilgisi * bilgi = harf in harfBilgileri_;
        return bilgi ? bilgi.büyüğü() : toUpper(harf);
    }

    /*
     * Harfin aksansızını döndürür
     *
     * Eğer alfabede bulunan bir harf ise alfabe kurallarını kullanır,
     * değilse harfin kendisini döndürür
     */
    dchar aksansızı(dchar harf) const
    {
        const AlfabeHarfiBilgisi * bilgi = harf in harfBilgileri_;
        return bilgi ? bilgi.aksansızı() : harf;
    }

    /*
     * Harfleri karşılaştırır ve sıra ilişkilerini döndürür
     *
     * Eğer iki harf de alfabede bulunuyorsa alfabe kurallarını
     * kullanır. Birisi alfabede bulunuyorsa, o öncedir. İkisi de alfabede
     * bulunmuyorsa, Unicode karşılaştırmasının sonucunu üretir.
     */
    Sıra sıra(dchar soldaki, dchar sağdaki) const
    {
        Sıra sonuç;

        const AlfabeHarfiBilgisi * soldakiBilgi = soldaki in harfBilgileri_;
        const AlfabeHarfiBilgisi * sağdakiBilgi = sağdaki in harfBilgileri_;

        if (soldakiBilgi) {
            sonuç = (sağdakiBilgi
                     ? soldakiBilgi.sıra(*sağdakiBilgi)  // ikisi de alfabede
                     : Sıra(-1, 0, 0));  // sağdaki yabancı; sonra gelmeli
        } else {
            sonuç = (sağdakiBilgi
                     ? Sıra(1, 0, 0)     // soldaki yabancı; sonra gelmeli
                     : Sıra(soldaki - sağdaki, 0, 0));   // ikisi de yabancı
        }

        return sonuç;
    }

    int karşılaştır(string kod)(const İm!kod[] soldaki,
                                const İm!kod[] sağdaki) const
    {
        /*
         * Aksan farkı bulunmadığı zaman eşit kabul edilmeleri için bunun
         * ilk değerinin 0 olması gerekiyor
         */
        int aksanSıra = 0;
        bool aksanSıraBulundu_mu = false;

        /*
         * Küçük/Büyük farkı bulunmadığı zaman eşit kabul edilmeleri için
         * bunun ilk değerinin 0 olması gerekiyor
         */
        int küçükBüyükSıra = 0;
        int küçükBüyükSıraBulundu_mu = false;

        for (int i = 0; true; ++i) {

            if (i == soldaki.length) {
                /*
                 * Birincisi tükendi. Eğer ikincisinde im kaldıysa eksi bir
                 * değer döndürmeliyiz; kalmadıysa, bulduğumuz ilk aksan
                 * farkına bakmalıyız. Eğer o da eşitse, kararı bulduğumuz
                 * ilk küçük/büyük farkına göre vereceğiz.
                 */
                const int uzunlukFarkı = i - cast(int)sağdaki.length;
                return üçlüSıraKarşılaştırması(uzunlukFarkı,
                                               aksanSıra,
                                               küçükBüyükSıra);
            }

            if (i == sağdaki.length) {
                /*
                 * İkincisi tükendi. Eğer birincisinde im kaldıysa artı bir
                 * değer döndürmeliyiz; kalmadıysa, bulduğumuz ilk aksan
                 * farkına bakmalıyız. Eğer o da eşitse, kararı bulduğumuz
                 * ilk küçük/büyük farkına göre vereceğiz.
                 */
                const int uzunlukFarkı = cast(int)soldaki.length - i;
                return üçlüSıraKarşılaştırması(uzunlukFarkı,
                                               aksanSıra,
                                               küçükBüyükSıra);
            }

            const sıra = soldaki[i].sıra(sağdaki[i]);

            if (sıra.temel) {
                /* Aralarında temel bir fark var; yeterli... */
                return sıra.temel;

            } else if (sıra.aksan) {
                /*
                 * Eğer şimdiye kadar aksan farkı bulunmadıysa, daha
                 * sonradan kullanmak üzere aklımızda tutmamız gerekiyor.
                 */
                if (!aksanSıraBulundu_mu) {
                    aksanSıra = sıra.aksan;
                    aksanSıraBulundu_mu = true;
                }
            } else if (sıra.küçükBüyük) {
                /*
                 * Aynı şekilde, belki de sıralamayı en sonunda belirleyen,
                 * bu küçük/büyük farkı olacak
                 */
                if (!küçükBüyükSıraBulundu_mu) {
                    küçükBüyükSıra = sıra.küçükBüyük;
                    küçükBüyükSıraBulundu_mu = true;
                }
            }
        }
    }

    string bilgi() const
    {
        string sonuç = "___ " ~ isim_ ~ " Alfabesi ___\n";

        foreach (harf; harfler_) {
            const sıraNumarası =
                harfBilgileri_[harf.küçük.kod].temelSıraNumarası;

            sonuç ~= format("%3s %s%s (%s)\n"
                            "       (%s)\n",
                            sıraNumarası,
                            harf.küçük, harf.büyük,
                            harf.küçük_aksanlılar, harf.büyük_aksanlılar);
        }

        foreach (kod, bilgi; harfBilgileri_) {
            sonuç ~= format(" %s:(%s)", kod, bilgi);
        }

        return sonuç;
    }

    @property string isim() const
    {
        return isim_;
    }

    string toString() const
    {
        string sonuç;

        foreach (harf; harfler_) {
            sonuç ~= format("%s%s", harf.küçük, harf.büyük);
        }

        return sonuç;
    }
}

unittest
{
    assert(türkAlfabesi.küçüğü('A') == 'a');
    assert(türkAlfabesi.küçüğü('I') == 'ı');
    assert(türkAlfabesi.küçüğü('Û') == 'û');

    assert(türkAlfabesi.büyüğü('q') == 'Q');
    assert(türkAlfabesi.büyüğü('i') == 'İ');
    assert(türkAlfabesi.büyüğü('é') == 'É');

    assert(türkAlfabesi.aksansızı('ç') == 'ç');
    assert(ingilizAlfabesi.aksansızı('ç') == 'c');
    assert(türkAlfabesi.aksansızı('â') == 'a');
    assert(türkAlfabesi.aksansızı('î') == 'ı');
    assert(ingilizAlfabesi.aksansızı('☺') == '☺');
    assert(türkAlfabesi.aksansızı('Ω') == 'Ω');

    Sıra gğ = türkAlfabesi.sıra('g', 'ğ');
    assert(gğ.temel < 0);

    Sıra âa = türkAlfabesi.sıra('â', 'a');
    assert(âa.temel == 0);
    assert(âa.aksan > 0);

    assert(türkAlfabesi.sıra('a', ' ').temel > 0);
    assert(türkAlfabesi.sıra('0', 'z').temel < 0);

    assert(türkAlfabesi.sıra('ş', 'Ş').temel == 0);
    assert(türkAlfabesi.sıra('ş', 'Ş').aksan == 0);

    assert(azeriAlfabesi.sıra('x', 'I').temel < 0);
    assert(azeriAlfabesi.sıra('Ə', 'e').temel > 0);

    assert(türkmenAlfabesi.sıra('e', 'ä').temel < 0);
    assert(türkmenAlfabesi.sıra('ä', 'f').temel < 0);

    assert(kırımTatarAlfabesi.sıra('ñ', 'O').temel < 0);
    assert(kırımTatarAlfabesi.sıra('â', 'a').temel == 0);
    assert(kırımTatarAlfabesi.sıra('â', 'a').aksan > 0);

    assert(gagavuzAlfabesi.sıra('a', 'ä').temel < 0);
    assert(gagavuzAlfabesi.sıra('ţ', 't').temel > 0);

    assert(kumukAlfabesi.sıra('n', 'ŋ').temel < 0);
    assert(kumukAlfabesi.sıra('ƣ', 'h').temel < 0);
    assert(kumukAlfabesi.sıra('ɵ', 'o').temel > 0);
    assert(kumukAlfabesi.sıra('Z', 'ƶ').temel < 0);
    assert(kumukAlfabesi.sıra('Ь', 'ƶ').temel > 0);
}

/*
 * Normal bir alfabe harfini kuran dizgiyi oluşturur
 *
 * Örnek: "AlfabeHarfi(a, A, a_aksan, A_aksan)"
 */
private dstring normal(dchar harf)
{
    return "AlfabeHarfi("d ~ harf ~ "_harfi, " ~ toUpper(harf) ~ "_harfi"
        ~ ", " ~ harf ~"_aksan"
        ~ ", " ~ toUpper(harf) ~"_aksan)";
}
unittest
{
    assert(normal('a') == "AlfabeHarfi(a_harfi, A_harfi, a_aksan, A_aksan)");
}

/*
 * Aksanlıları bulunmayan bir alfabe harfi kuran dizgiyi oluşturur
 *
 * Örnek: "AlfabeHarfi(c, C, Grup(), Grup())"
 */
private dstring aksansız(dchar harf)
{
    return "AlfabeHarfi("d ~ harf ~ "_harfi, " ~ toUpper(harf) ~ "_harfi"
        ~ ", Grup(), Grup())";
}
unittest
{
    assert(aksansız('c') == "AlfabeHarfi(c_harfi, C_harfi, Grup(), Grup())");
}

/*
 * 'çıkan'ın ve bütün aksanlılarının, 'harf'ten çıkartılmışını kuran bir
 * dizgi oluşturur
 */
private dstring çıkart(dchar harf, dchar çıkan)
{
    return "AlfabeHarfi("d ~ harf ~ "_harfi, " ~ toUpper(harf) ~ "_harfi"
        ~ ", " ~ harf ~ "_aksan - " ~ çıkan ~ "_aksan - " ~ çıkan ~ "_harfi"
        ~ ", " ~ toUpper(harf) ~ "_aksan - "
        ~ toUpper(çıkan) ~ "_aksan - " ~ toUpper(çıkan) ~ "_harfi)";
}
unittest
{
    assert(çıkart('c', 'ç') ==
           "AlfabeHarfi(c_harfi, C_harfi, "
           "c_aksan - ç_aksan - ç_harfi, C_aksan - Ç_aksan - Ç_harfi)");
}

/*
 * Belirtilen aksanlı harfi normal harf haline getiren dizgiyi oluşturur
 *
 * Aksanlı harfi asıl harfin aksanlıları arasından çıkartır ve geri
 * kalanını kendi aksanlıları olarak kullanır
 *
 * Örnek: "AlfabeHarfi(ç, Ç, c_aksan - ç, C_aksan - Ç)"
 */
private dstring aksandanHarfe(dchar aksanlı, dchar harf)
{
    return "AlfabeHarfi("d ~ aksanlı ~ "_harfi, " ~ toUpper(aksanlı) ~ "_harfi"
        ~ ", " ~ harf ~"_aksan - " ~ aksanlı ~ "_harfi"
        ~ ", " ~ toUpper(harf) ~"_aksan - " ~ toUpper(aksanlı) ~ "_harfi)";
}
unittest
{
    assert(aksandanHarfe('ç', 'c') ==
           "AlfabeHarfi(ç_harfi, Ç_harfi, "
           "c_aksan - ç_harfi, C_aksan - Ç_harfi)");
}

private dstring boşluk()
{
    return "AlfabeHarfi(boşluk_harfi, boşluk_harfi, Grup(), Grup())";
}

private dstring rakam(dchar rakamKarakteri)
{
    return "AlfabeHarfi(Harf('"d ~ rakamKarakteri
        ~ "'), Harf('" ~ rakamKarakteri
        ~ "'), Grup(), Grup())";
}

unittest
{
    assert(rakam('5') == "AlfabeHarfi(Harf('5'), Harf('5'), Grup(), Grup())");
}

private dstring türk_ı()
{
    return "AlfabeHarfi(ı_harfi, I_harfi, tr_ı_aksan, tr_I_aksan)";
}

private dstring türk_i()
{
    return "AlfabeHarfi(i_harfi, İ_harfi, tr_i_aksan, tr_İ_aksan)";
}

/*
 * Daha sonradan çalışma zamanında kullanacağı bilgileri harflerden
 * edinir
 */
AlfabeHarfiBilgisi[dchar] bilgiTopla(string isim, AlfabeHarfi[] harfler)
out (sonuç)
{
    /*
     * Alfabenin kendisinde bulunmasa bile bütün Latin harflerinin
     * tanıtılmış olmalarını istiyoruz
     */
    foreach (harf; "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") {
        assert(harf in sonuç,
               to!string(harf) ~ ", " ~ isim ~
               " alfabesinde belirtilmemiş");
    }
}
body
{
    AlfabeHarfiBilgisi[dchar] bilgiler;

    foreach (int temelSıraNumarası, ref harf; harfler) {

        // Küçük harfini tanı
        tanı(isim, bilgiler, harf.küçük.kod, harf, harf.küçük,
             temelSıraNumarası, 0, KüçükBüyük.küçük);

        // Büyük harfini tanı
        tanı(isim, bilgiler, harf.büyük.kod, harf, harf.büyük,
             temelSıraNumarası, 0, KüçükBüyük.büyük);

        // Aksanlı harflerin küçüklerini tanı
        foreach (int aksanlıİndeksi, ref aksanlı; harf.küçük_aksanlılar.grup) {
            immutable aksanSıraNumarası = aksanlıİndeksi + 1;

            tanı(isim, bilgiler, aksanlı, harf, harf.küçük,
                 temelSıraNumarası, aksanSıraNumarası, KüçükBüyük.küçük);
        }

        // Aksanlı harflerin büyüklerini tanı
        foreach (int aksanlıİndeksi, ref aksanlı; harf.büyük_aksanlılar.grup) {
            immutable aksanSıraNumarası = aksanlıİndeksi + 1;

            tanı(isim, bilgiler, aksanlı, harf, harf.büyük,
                 temelSıraNumarası, aksanSıraNumarası, KüçükBüyük.büyük);
        }
    }

    return bilgiler;
}

/*
 * Tek bir harfi "tanır". Ayrı bir işlev olmasının en önemli nedeni, in
 * bloğunun her bir harf için çalıştırılacak olmasıdır
 */
private void tanı(string isim,
                  ref AlfabeHarfiBilgisi[dchar] bilgiler,
                  dchar kod,
                  ref const AlfabeHarfi harf,
                  Harf aksansızı,
                  int temelSıraNumarası,
                  int aksanSıraNumarası,
                  KüçükBüyük küçükBüyük)
in
{
    /*
     * Aynı harfin birden fazla geçmesini engellemek istiyoruz
     */

    const mevcutBilgi = kod in bilgiler;

    /*
     * Bazı harflerin küçük/büyük ayrımı bulunmadığı için onları
     * hoş görüyoruz
     */
    if (mevcutBilgi) {

        const bool küçükBüyükAynı = (harf.küçük == harf.büyük);

        assert (küçükBüyükAynı   // boşluk, rakam, vs.
                ||
                ((kod != mevcutBilgi.harf.küçük.kod)
                 &&
                 (kod != mevcutBilgi.harf.büyük.kod)),
                format("%s, %s alfabesinin %s harfinde zaten var",
                       kod, isim, mevcutBilgi.harf.küçük));
    }
}
body
{
    // İlk tanıtımı yeterli; üstüne yazmaya gerek yok
    if (!(kod in bilgiler)) {
        bilgiler[kod] = AlfabeHarfiBilgisi(&harf,
                                           aksansızı,
                                           temelSıraNumarası,
                                           aksanSıraNumarası,
                                           küçükBüyük);
    }
}

/*
 * XXX: Aşağıdaki ingilizAlfabesi, türkAlfabesi, vs. evrensel nesnelerini
 *      ve onlara erişim sağlayan alfabe!kod() işlevlerini gerektirmeyen
 *      bir çözüm bulabilir miyiz?
 */

enum ingilizAlfabesiHam =
    [
        mixin(boşluk()),
        mixin(rakam('0')),
        mixin(rakam('1')),
        mixin(rakam('2')),
        mixin(rakam('3')),
        mixin(rakam('4')),
        mixin(rakam('5')),
        mixin(rakam('6')),
        mixin(rakam('7')),
        mixin(rakam('8')),
        mixin(rakam('9')),
        mixin(normal('a')),
        mixin(normal('b')),
        mixin(normal('c')),
        mixin(normal('d')),
        mixin(normal('e')),
        mixin(normal('f')),
        mixin(normal('g')),
        mixin(normal('h')),
        mixin(normal('i')),
        mixin(normal('j')),
        mixin(normal('k')),
        mixin(normal('l')),
        mixin(normal('m')),
        mixin(normal('n')),
        mixin(normal('o')),
        mixin(normal('p')),
        mixin(normal('q')),
        mixin(normal('r')),
        mixin(normal('s')),
        mixin(normal('t')),
        mixin(normal('u')),
        mixin(normal('v')),
        mixin(normal('w')),
        mixin(normal('x')),
        mixin(normal('y')),
        mixin(normal('z'))
     ];

Alfabe ingilizAlfabesi;
static this()
{
ingilizAlfabesi =
    Alfabe("İngiliz", ingilizAlfabesiHam,
           bilgiTopla("İngiliz", ingilizAlfabesiHam));
}

enum türkAlfabesiHam = [
        mixin(boşluk()),
        mixin(rakam('0')),
        mixin(rakam('1')),
        mixin(rakam('2')),
        mixin(rakam('3')),
        mixin(rakam('4')),
        mixin(rakam('5')),
        mixin(rakam('6')),
        mixin(rakam('7')),
        mixin(rakam('8')),
        mixin(rakam('9')),
        mixin(normal('a')),
        mixin(normal('b')),
        mixin(çıkart('c', 'ç')),
        mixin(normal('ç')),
        mixin(normal('d')),
        mixin(normal('e')),
        mixin(normal('f')),
        mixin(çıkart('g', 'ğ')),
        mixin(normal('ğ')),
        mixin(normal('h')),
        mixin(türk_ı()),
        mixin(türk_i()),
        mixin(normal('j')),
        mixin(normal('k')),
        mixin(normal('l')),
        mixin(normal('m')),
        mixin(normal('n')),
        mixin(çıkart('o', 'ö')),
        mixin(normal('ö')),
        mixin(normal('p')),
        mixin(normal('q')),
        mixin(normal('r')),
        mixin(çıkart('s', 'ş')),
        mixin(normal('ş')),
        mixin(normal('t')),
        mixin(çıkart('u', 'ü')),
        mixin(normal('ü')),
        mixin(normal('v')),
        mixin(normal('w')),
        mixin(normal('x')),
        mixin(normal('y')),
        mixin(normal('z'))
     ];

Alfabe türkAlfabesi;
static this()
{
türkAlfabesi =
    Alfabe("Türk", türkAlfabesiHam, bilgiTopla("Türk", türkAlfabesiHam));
}

enum azeriAlfabesiHam =
    [
        mixin(boşluk()),
        mixin(rakam('0')),
        mixin(rakam('1')),
        mixin(rakam('2')),
        mixin(rakam('3')),
        mixin(rakam('4')),
        mixin(rakam('5')),
        mixin(rakam('6')),
        mixin(rakam('7')),
        mixin(rakam('8')),
        mixin(rakam('9')),
        mixin(normal('a')),
        mixin(normal('b')),
        mixin(çıkart('c', 'ç')),
        mixin(normal('ç')),
        mixin(normal('d')),
        mixin(normal('e')),
        AlfabeHarfi(ə_harfi, Ə_harfi, Grup(), Grup()),
        mixin(normal('f')),
        mixin(çıkart('g', 'ğ')),
        mixin(normal('ğ')),
        mixin(normal('h')),
        mixin(normal('x')),
        mixin(türk_ı()),
        mixin(türk_i()),
        mixin(normal('j')),
        mixin(normal('k')),
        mixin(normal('q')),
        mixin(normal('l')),
        mixin(normal('m')),
        mixin(normal('n')),
        mixin(çıkart('o', 'ö')),
        mixin(normal('ö')),
        mixin(normal('p')),
        mixin(normal('r')),
        mixin(çıkart('s', 'ş')),
        mixin(normal('ş')),
        mixin(normal('t')),
        mixin(çıkart('u', 'ü')),
        mixin(normal('ü')),
        mixin(normal('v')),
        mixin(normal('w')),
        mixin(normal('y')),
        mixin(normal('z'))
     ];

Alfabe azeriAlfabesi;
static this()
{
azeriAlfabesi =
    Alfabe("Azeri", azeriAlfabesiHam, bilgiTopla("Azeri", azeriAlfabesiHam));
}

enum türkmenAlfabesiHam =
    [
        mixin(boşluk()),
        mixin(rakam('0')),
        mixin(rakam('1')),
        mixin(rakam('2')),
        mixin(rakam('3')),
        mixin(rakam('4')),
        mixin(rakam('5')),
        mixin(rakam('6')),
        mixin(rakam('7')),
        mixin(rakam('8')),
        mixin(rakam('9')),
        AlfabeHarfi(a_harfi, A_harfi, a_aksan - ä_harfi, A_aksan - Ä_harfi),
        mixin(normal('b')),
        AlfabeHarfi(ç_harfi, Ç_harfi, c_harfi ~ (c_aksan - ç_harfi),
                    C_harfi ~ (C_aksan - Ç_harfi)),
        mixin(normal('d')),
        mixin(normal('e')),
        AlfabeHarfi(ä_harfi, Ä_harfi, Grup(), Grup()),
        mixin(normal('f')),
        mixin(normal('g')),
        mixin(normal('h')),
        mixin(normal('i')),
        mixin(normal('j')),
        AlfabeHarfi(ž_harfi, Ž_harfi, Grup(), Grup()),
        mixin(normal('k')),
        mixin(normal('l')),
        mixin(normal('m')),
        mixin(aksansız('n')),
        mixin(aksandanHarfe('ň', 'n')),
        mixin(çıkart('o', 'ö')),
        mixin(normal('ö')),
        mixin(normal('p')),
        mixin(normal('q')),
        mixin(normal('r')),
        mixin(çıkart('s', 'ş')),
        mixin(normal('ş')),
        mixin(normal('t')),
        mixin(çıkart('u', 'ü')),
        mixin(normal('ü')),
        mixin(normal('v')),
        mixin(normal('w')),
        mixin(normal('x')),
        mixin(aksansız('y')),
        mixin(aksandanHarfe('ý', 'y')),
        AlfabeHarfi(z_harfi, Z_harfi, z_aksan - ž_harfi, Z_aksan - Ž_harfi)
     ];

Alfabe türkmenAlfabesi;
static this()
{
türkmenAlfabesi =
    Alfabe("Türkmen", türkmenAlfabesiHam,
           bilgiTopla("Türkmen", türkmenAlfabesiHam));
}

enum kırımTatarAlfabesiHam =
    [
        mixin(boşluk()),
        mixin(rakam('0')),
        mixin(rakam('1')),
        mixin(rakam('2')),
        mixin(rakam('3')),
        mixin(rakam('4')),
        mixin(rakam('5')),
        mixin(rakam('6')),
        mixin(rakam('7')),
        mixin(rakam('8')),
        mixin(rakam('9')),
        mixin(normal('a')),
        mixin(normal('b')),
        mixin(çıkart('c', 'ç')),
        mixin(normal('ç')),
        mixin(normal('d')),
        mixin(normal('e')),
        mixin(normal('f')),
        mixin(çıkart('g', 'ğ')),
        mixin(normal('ğ')),
        mixin(normal('h')),
        mixin(türk_ı()),
        mixin(türk_i()),
        mixin(normal('j')),
        mixin(normal('k')),
        mixin(normal('l')),
        mixin(normal('m')),
        mixin(aksansız('n')),
        mixin(aksandanHarfe('ñ', 'n')),
        mixin(çıkart('o', 'ö')),
        mixin(normal('ö')),
        mixin(normal('p')),
        mixin(normal('q')),
        mixin(normal('r')),
        mixin(çıkart('s', 'ş')),
        mixin(normal('ş')),
        mixin(normal('t')),
        mixin(çıkart('u', 'ü')),
        mixin(normal('ü')),
        mixin(normal('v')),
        mixin(normal('w')),
        mixin(normal('x')),
        mixin(normal('y')),
        mixin(normal('z'))
     ];

Alfabe kırımTatarAlfabesi;
static this()
{
kırımTatarAlfabesi =
    Alfabe("Kırım Tatar", kırımTatarAlfabesiHam,
           bilgiTopla("Kırım Tatar", kırımTatarAlfabesiHam));
}

enum gagavuzAlfabesiHam =
    [
        mixin(boşluk()),
        mixin(rakam('0')),
        mixin(rakam('1')),
        mixin(rakam('2')),
        mixin(rakam('3')),
        mixin(rakam('4')),
        mixin(rakam('5')),
        mixin(rakam('6')),
        mixin(rakam('7')),
        mixin(rakam('8')),
        mixin(rakam('9')),
        mixin(aksansız('a')),
        mixin(aksandanHarfe('ä', 'a')),
        mixin(normal('b')),
        mixin(çıkart('c', 'ç')),
        mixin(normal('ç')),
        mixin(normal('d')),
        mixin(normal('e')),
        mixin(normal('f')),
        mixin(normal('g')),
        mixin(normal('h')),
        mixin(türk_ı()),
        mixin(türk_i()),
        mixin(normal('j')),
        mixin(normal('k')),
        mixin(normal('l')),
        mixin(normal('m')),
        mixin(normal('n')),
        mixin(çıkart('o', 'ö')),
        mixin(normal('ö')),
        mixin(normal('p')),
        mixin(normal('q')),
        mixin(normal('r')),
        mixin(çıkart('s', 'ş')),
        mixin(normal('ş')),
        mixin(aksansız('t')),
        mixin(aksandanHarfe('ţ', 't')),
        mixin(çıkart('u', 'ü')),
        mixin(normal('ü')),
        mixin(normal('v')),
        mixin(normal('w')),
        mixin(normal('x')),
        mixin(normal('y')),
        mixin(normal('z'))
     ];

Alfabe gagavuzAlfabesi;
static this()
{
gagavuzAlfabesi =
    Alfabe("Gagavuz", gagavuzAlfabesiHam,
           bilgiTopla("Gagavuz", gagavuzAlfabesiHam));
}

enum kumukAlfabesiHam =
    [
        mixin(boşluk()),
        mixin(rakam('0')),
        mixin(rakam('1')),
        mixin(rakam('2')),
        mixin(rakam('3')),
        mixin(rakam('4')),
        mixin(rakam('5')),
        mixin(rakam('6')),
        mixin(rakam('7')),
        mixin(rakam('8')),
        mixin(rakam('9')),
        mixin(normal('a')),
        mixin(normal('b')),
        mixin(çıkart('c', 'ç')),
        mixin(normal('ç')),
        mixin(normal('d')),
        mixin(normal('e')),
        mixin(normal('f')),
        mixin(normal('g')),
        AlfabeHarfi(ƣ_harfi, Ƣ_harfi, Grup(), Grup()),
        mixin(normal('h')),
        mixin(normal('i')),
        mixin(normal('j')),
        mixin(normal('k')),
        mixin(normal('l')),
        mixin(normal('m')),
        mixin(aksansız('n')),
        AlfabeHarfi(ŋ_harfi, Ŋ_harfi, n_aksan, N_aksan),
        mixin(normal('o')),
        AlfabeHarfi(ɵ_harfi, Ɵ_harfi, Grup(), Grup()),
        mixin(normal('p')),
        mixin(normal('q')),
        mixin(normal('r')),
        mixin(çıkart('s', 'ş')),
        mixin(normal('ş')),
        mixin(normal('t')),
        mixin(normal('u')),
        mixin(normal('v')),
        mixin(normal('w')),
        mixin(normal('x')),
        mixin(normal('y')),
        mixin(aksansız('z')),
        AlfabeHarfi(ƶ_harfi, Ƶ_harfi, z_aksan - ƶ_harfi, Z_aksan - Ƶ_harfi),
        AlfabeHarfi(ь_harfi, Ь_harfi, Grup(), Grup())
     ];

Alfabe kumukAlfabesi;
static this()
{
kumukAlfabesi =
    Alfabe("Kumuk", kumukAlfabesiHam, bilgiTopla("Kumuk", kumukAlfabesiHam));
}

enum hakasAlfabesiHam =
    [
        mixin(boşluk()),
        mixin(rakam('0')),
        mixin(rakam('1')),
        mixin(rakam('2')),
        mixin(rakam('3')),
        mixin(rakam('4')),
        mixin(rakam('5')),
        mixin(rakam('6')),
        mixin(rakam('7')),
        mixin(rakam('8')),
        mixin(rakam('9')),
        mixin(normal('a')),
        mixin(normal('b')),
        mixin(çıkart('c', 'ç')),
        mixin(normal('ç')),
        mixin(normal('d')),
        AlfabeHarfi(ə_harfi, Ə_harfi, Grup(), Grup()),
        mixin(normal('e')),  // aslında bu alfabede bulunmuyor
        mixin(normal('f')),
        mixin(normal('g')),
        AlfabeHarfi(ƣ_harfi, Ƣ_harfi, Grup(), Grup()),
        mixin(normal('h')),  // aslında bu alfabede bulunmuyor
        mixin(aksansız('i')),
        mixin(aksandanHarfe('į', 'i')),
        mixin(normal('j')),
        mixin(normal('k')),
        mixin(normal('l')),
        mixin(normal('m')),
        mixin(aksansız('n')),
        AlfabeHarfi(ŋ_harfi, Ŋ_harfi, n_aksan, N_aksan),
        mixin(normal('o')),
        AlfabeHarfi(ɵ_harfi, Ɵ_harfi, Grup(), Grup()),
        mixin(normal('p')),
        mixin(normal('q')),
        mixin(normal('r')),
        mixin(çıkart('s', 'ş')),
        mixin(normal('ş')),
        mixin(normal('t')),
        mixin(normal('u')),
        mixin(normal('v')),
        mixin(normal('w')),
        mixin(normal('x')),
        mixin(normal('y')),
        mixin(aksansız('z')),
        AlfabeHarfi(ƶ_harfi, Ƶ_harfi, z_aksan - ƶ_harfi, Z_aksan - Ƶ_harfi),
        AlfabeHarfi(ь_harfi, Ь_harfi, Grup(), Grup())
     ];

Alfabe hakasAlfabesi;
static this()
{
hakasAlfabesi =
    Alfabe("Hakas", hakasAlfabesiHam, bilgiTopla("Hakas", hakasAlfabesiHam));
}

enum kürtAlfabesiHam =
    [
        mixin(boşluk()),
        mixin(rakam('0')),
        mixin(rakam('1')),
        mixin(rakam('2')),
        mixin(rakam('3')),
        mixin(rakam('4')),
        mixin(rakam('5')),
        mixin(rakam('6')),
        mixin(rakam('7')),
        mixin(rakam('8')),
        mixin(rakam('9')),
        mixin(normal('a')),
        mixin(normal('b')),
        mixin(çıkart('c', 'ç')),
        mixin(normal('ç')),
        mixin(normal('d')),
        mixin(aksansız('e')),
        mixin(aksandanHarfe('ê', 'e')),
        mixin(normal('f')),
        mixin(normal('g')),
        mixin(aksansız('h')),
        AlfabeHarfi(ḧ_harfi, Ḧ_harfi, h_aksan - ḧ_harfi, H_aksan - Ḧ_harfi),
        mixin(aksansız('i')),
        mixin(aksandanHarfe('î', 'i')),
        mixin(normal('j')),
        mixin(normal('k')),
        mixin(normal('l')),
        mixin(normal('m')),
        mixin(normal('n')),
        mixin(normal('o')),
        mixin(normal('p')),
        mixin(normal('q')),
        mixin(normal('r')),
        mixin(çıkart('s', 'ş')),
        mixin(normal('ş')),
        mixin(normal('t')),
        mixin(aksansız('u')),
        mixin(aksandanHarfe('û', 'u')),
        mixin(normal('v')),
        mixin(normal('w')),
        mixin(aksansız('x')),
        AlfabeHarfi(ẍ_harfi, Ẍ_harfi, x_aksan - ẍ_harfi, X_aksan - Ẍ_harfi),
        mixin(normal('y')),
        mixin(normal('z'))
     ];

Alfabe kürtAlfabesi;
static this()
{
kürtAlfabesi =
    Alfabe("Kürt", kürtAlfabesiHam, bilgiTopla("Kürt", kürtAlfabesiHam));
}

enum irlandaAlfabesiHam =
    [
        mixin(boşluk()),
        mixin(rakam('0')),
        mixin(rakam('1')),
        mixin(rakam('2')),
        mixin(rakam('3')),
        mixin(rakam('4')),
        mixin(rakam('5')),
        mixin(rakam('6')),
        mixin(rakam('7')),
        mixin(rakam('8')),
        mixin(rakam('9')),
        mixin(aksansız('a')),
        mixin(aksandanHarfe('á', 'a')),
        mixin(normal('b')),
        mixin(normal('c')),
        mixin(normal('d')),
        mixin(aksansız('e')),
        mixin(aksandanHarfe('é', 'e')),
        mixin(normal('f')),
        mixin(normal('g')),
        mixin(normal('h')),
        AlfabeHarfi(ı_harfi, I_harfi, Grup(), Grup()),
        AlfabeHarfi(í_harfi, Í_harfi,
                    i_harfi ~ i_aksan - í_harfi - ı_harfi,
                    i_harfi ~ I_aksan - Í_harfi - ı_harfi),
        mixin(normal('j')),
        mixin(normal('k')),
        mixin(normal('l')),
        mixin(normal('m')),
        mixin(normal('n')),
        mixin(aksansız('o')),
        mixin(aksandanHarfe('ó', 'o')),
        mixin(normal('p')),
        mixin(normal('q')),
        mixin(normal('r')),
        mixin(normal('s')),
        mixin(normal('t')),
        mixin(aksansız('u')),
        mixin(aksandanHarfe('ú', 'u')),
        mixin(normal('v')),
        mixin(normal('w')),
        mixin(normal('x')),
        mixin(normal('y')),
        mixin(normal('z'))
     ];

Alfabe irlandaAlfabesi;
static this()
{
irlandaAlfabesi =
    Alfabe("İrlanda", irlandaAlfabesiHam,
           bilgiTopla("İrlanda", irlandaAlfabesiHam));
}

/*
 * Not: Aşağıdaki işlevlerin dönüş türlerini basitçe 'auto' yazmak isterdik;
 *      ama öyle yapınca Ddoc belgesini üretmiyor
 */

/**
 * Alfabe kodundan, o kodun alfabesine derleme zamanında dönüşüm sağlar
 *
 * Böylece asıl programda, özellikle başka şablonlar içindeyken, asıl
 * alfabenin ismini bilmek zorunda kalmadan, örneğin alfabe!kod yazarak o
 * koda ait alfabeye erişilebilir
 *
 * Alfabe kodları olarak, bire bir karşılık olmasalar da ISO 639-3 dil
 * kodlarını kullanıyoruz
 */
const(Alfabe) * alfabe(string kod : "eng")()
{
    return &ingilizAlfabesi;
}

/** ditto */
const(Alfabe) * alfabe(string kod : "tur")()
{
    return &türkAlfabesi;
}

/** ditto */
const(Alfabe) * alfabe(string kod : "aze")()
{
    return &azeriAlfabesi;
}

/** ditto */
const(Alfabe) * alfabe(string kod : "tuk")()
{
    return &türkmenAlfabesi;
}

/** ditto */
const(Alfabe) * alfabe(string kod : "crh")()
{
    return &kırımTatarAlfabesi;
}

/** ditto */
const(Alfabe) * alfabe(string kod : "gag")()
{
    return &gagavuzAlfabesi;
}

/** ditto */
const(Alfabe) * alfabe(string kod : "kum")()
{
    return &kumukAlfabesi;
}

/** ditto */
const(Alfabe) * alfabe(string kod : "kjh")()
{
    return &hakasAlfabesi;
}

/** ditto */
const(Alfabe) * alfabe(string kod : "kur")()
{
    return &kürtAlfabesi;
}

/** ditto */
const(Alfabe) * alfabe(string kod : "gle")()
{
    return &irlandaAlfabesi;
}

unittest
{
    debug(tr_alfabe)
    {
        writeln(*alfabe!"eng");
        writeln(alfabe!"eng".bilgi);

        writeln(*alfabe!"tur");
        writeln(alfabe!"tur".bilgi);

        writeln(*alfabe!"aze");
        writeln(alfabe!"aze".bilgi);

        writeln(*alfabe!"tuk");
        writeln(alfabe!"tuk".bilgi);

        writeln(*alfabe!"crh");
        writeln(alfabe!"crh".bilgi);

        writeln(*alfabe!"gag");
        writeln(alfabe!"gag".bilgi);

        writeln(*alfabe!"kum");
        writeln(alfabe!"kum".bilgi);

        writeln(*alfabe!"kjh");
        writeln(alfabe!"kjh".bilgi);

        writeln(*alfabe!"kur");
        writeln(alfabe!"kur".bilgi);

        writeln(*alfabe!"gle");
        writeln(alfabe!"gle".bilgi);
    }
}

immutable(Alfabe[string]) alfabeler;

static this()
{
    alfabeler = cast(immutable(Alfabe[string]))[
        "eng" : *alfabe!"eng",
        "tur" : *alfabe!"tur",
        "aze" : *alfabe!"aze",
        "tuk" : *alfabe!"tuk",
        "crh" : *alfabe!"crh",
        "gag" : *alfabe!"gag",
        "kum" : *alfabe!"kum",
        "kjh" : *alfabe!"kjh",
        "kur" : *alfabe!"kur",
        "gle" : *alfabe!"gle",
    ];
}
/**
 * Alfabe kodundan o kodun alfabesine çalışma zamanında dönüşüm sağlar
 *
 * Alfabe kodları olarak, bire bir karşılık olmasalar da ISO 639-3 dil
 * kodlarını kullanıyoruz
 */
const(Alfabe) alfabe_dinamik(string kod)
{
    return alfabeler[kod];
}

unittest
{
    auto a = alfabe_dinamik("tur");
    assert(a.küçüğü('I') == 'ı');
}
