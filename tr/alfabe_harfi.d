module tr.alfabe_harfi;

import tr.harf;
import tr.grup;

import std.string;

/**
 * Kullanıcının alfabeleri tanımlarken kurması gereken bir alfabe harfi
 *
 * Harfin küçüğünü, büyüğünü, ve onların aksanlılarını bir arada sunar
 */
struct AlfabeHarfi
{
    Harf küçük;
    Harf büyük;
    Grup küçük_aksanlılar;
    Grup büyük_aksanlılar;

    this(Harf küçük,
         Harf büyük,
         Grup küçük_aksanlılar,
         Grup büyük_aksanlılar)
    in
    {
        assert(küçük_aksanlılar.grup.length == büyük_aksanlılar.grup.length,
               format("%s harfinin aksanlılarının uzunlukları farklı", küçük));
    }
    body
    {
        this.küçük = küçük;
        this.büyük = büyük;
        this.küçük_aksanlılar = küçük_aksanlılar;
        this.büyük_aksanlılar = büyük_aksanlılar;
    }

    string toString() const
    {
        return format("%s%s (%s %s)", küçük, büyük,
                      küçük_aksanlılar, büyük_aksanlılar);
    }
}

unittest
{
    import std.conv;

    auto h = AlfabeHarfi(Harf('a'), Harf('A'), Grup(), Grup());
    assert(to!string(h) == "aA ( )");
}
