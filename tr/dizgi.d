/**
 * Bu modül, bir alfabeye bağlı olan dizgi kavramını temsil eder
 *
 */
module tr.dizgi;

import tr.yazi_parcasi;

import tr.alfabe;
import tr.im;
import std.conv;
import std.range;
import std.exception;
import std.traits;

debug(tr_dizgi) import std.stdio;

/**
 * Belirli bir alfabenin imlerinden oluşan bir dizgiyi temsil eder
 *
 * Bir İm dizisi gibi işlem görse de, kolaylık açısından D'nin karakter ve
 * dizgi türleriyle de etkileşebilir.
 *
 * Küçük/Büyük harf dönüşümlerini ve sıra karşılaştırmalarını bağlı olduğu
 * alfabenin kurallarına göre yapar.
 *
 * Sıra karşılaştırmalarında öncelikle harflerin temel farklılıklarına
 * bakar; dizgilerin uzunlukları, aksanları, ve küçük/büyük farklılıkları
 * öncelikle gözardı edilir ('a' ve 'â' aynı sıradadır, 'C' ve 'c' aynı
 * sıradadır.) Eğer karakterler temelde aynıysa, dizgilerin uzunluklarına
 * bakılır; o da olmazsa aksanlara; ve o da olmazsa küçük/büyük harf
 * farklılıklarına.
 */
struct Dizgi(string kod)
{
private:

    alias İm!kod İmT;

    İmT[] imler_;

public:

    this(T)(T dizgi)
        if (isSomeString!T)
    {
        foreach (karakter; stride(dizgi, 1)) {
            imler_ ~= İmT(karakter);
        }
    }

    this(T)(T imler)
        if (is (T : İmT[]))
    {
        imler_ ~= imler;
    }

    /**
     * Dizideki im adedi
     */
    @property size_t length() const
    {
        return imler_.length;
    }

    /**
     * İçindeki karakterlere, değiştirmeye kapalı erişim sağlar
     */
    @property const(dchar)[] karakterler() const
    {
        /*
         * Bu dönüşüm tehlikeli kabul edilmelidir; çünkü İm'in tanımı
         * ileride değişirse çalışmaz. Güvenlik açısından hiç olmazsa bir
         * denetim kullanıyoruz.
         */
        static assert((İmT).sizeof == dchar.sizeof);

        return *cast(const(dchar)[] *)(&imler_);
    }

    /**
     * string olarak ifadesi
     */
    string toString() const
    {
        string sonuç;

        foreach (im; imler_) {
            sonuç ~= cast(dchar)im;
        }

        return text(sonuç);
    }

    /**
     * Eşleme tablosu indeks değeri
     */
    hash_t toHash() const nothrow @safe
    {
        hash_t değer;

        foreach (im; imler_) {
            değer += cast(dchar)im;
        }

        return değer;
    }

    /**
     * Eşitlik karşılaştırması
     *
     * Temel dizgilerde olduğu gibi, aksanlar ve küçük/büyük farklılıkları
     * önemlidir
     */
    bool opEquals(T)(const T dizgi) const
        if (is(T : const(dchar[])) || is(T : const(İmT[])))
    {
        if (imler_.length != dizgi.length) {
            return false;
        }

        foreach (i, karakter; dizgi) {
            if (imler_[i] != karakter) {
                return false;
            }
        }

        return true;
    }

    /** ditto */
    bool opEquals(T)(const T dizgi) const
        if (isNarrowString!T)
    {
        size_t i = 0;
        foreach (karakter; stride(dizgi, 1)) {
            if ((i >= imler_.length) || (imler_[i] != karakter)) {
                return false;
            }
            ++i;
        }

        return true;
    }

    /** ditto */
    bool opEquals(T)(T sağdaki) const
        if (is (T : const(Dizgi!kod)))
    {
        return imler_ == sağdaki.imler_;
    }

    /**
     * Dört aşama ile karşılaştırır:
     *
     * - öncelikle, temelde farklı olan ilk ime bakılır
     * - yoksa, uzun olan dizgi daha sonradır
     * - eşit uzunlukta iseler, aksanda farklılık gösteren ilk ime bakılır
     * - küçük/büyük olarak farklı olan ilk ime bakılır
     */
    int opCmp(const ref Dizgi!kod sağdaki) const
    {
        return alfabe!kod.karşılaştır(imler_, sağdaki.imler_);
    }

    int opCmp(Dizgi!kod sağdaki) const
    {
        return alfabe!kod.karşılaştır(imler_, sağdaki.imler_);
    }

    /**
     * Belirtilen indeksteki im değerini döndürür
     */
    ref İmT opIndex(size_t indeks)
    {
        return imler_[indeks];
    }

    /**
     * Belirtilen indeksteki imi değiştirir
     */
    void opIndexAssign(dchar karakter, size_t indeks)
    {
        imler_[indeks] = karakter;
    }

    /** ditto */
    void opIndexAssign(İmT im, size_t indeks)
    {
        imler_[indeks] = cast(dchar)im;
    }

    /**
     * Dizginin değiştirilebilen bir kopyasını döndürür
     */
    Dizgi dup() const
    {
        return Dizgi(imler_.dup);
    }

    /**
     * Dizginin değişmez bir kopyasını döndürür
     */
    immutable(Dizgi) idup() const
    {
        auto kopya = Dizgi(imler_.dup);
        auto sonuç = cast(immutable)(kopya);
        return sonuç;
    }

    /**
     * Belirtilen imi dizginin sonuna ekler
     */
    Dizgi opOpAssign(string işleç, T : dchar)(T karakter)
        if (işleç == "~")
    {
        imler_ ~= İmT(karakter);
        return this;
    }

    /** ditto */
    Dizgi opOpAssign(string işleç, T : İmT)(T im)
        if (işleç == "~")
    {
        imler_ ~= im;
        return this;
    }

    /**
     * Belirtilen dizgiyi bu dizginin sonuna ekler
     */
    Dizgi opOpAssign(string işleç, T : dchar[])(T dizgi)
        if (işleç == "~")
    {
        foreach (karakter; dizgi) {
            imler_ ~= İmT(karakter);
        }
        return this;
    }

    /** ditto */
    Dizgi opOpAssign(string işleç, T : wchar[])(T dizgi)
        if (işleç == "~")
    {
        return this ~= dtext(dizgi);
    }

    /** ditto */
    Dizgi opOpAssign(string işleç, T : İmT[])(T imler)
        if (işleç == "~")
    {
        this.imler_ ~= imler;
        return this;
    }

    /** ditto */
    Dizgi opOpAssign(string işleç, T : Dizgi)(T dizgi)
        if (işleç == "~")
    {
        this.imler_ ~= dizgi.imler_;
        return this;
    }

    /**
     * Sonuna belirtilen dizgi eklenmiş olan yeni bir dizgi döndürür
     */
    Dizgi opBinary(string işleç, T : Dizgi)(T dizgi) const
        if (işleç == "~")
    {
        Dizgi sonuç = this.dup;
        sonuç ~= dizgi.imler_;
        return sonuç;
    }

    /** ditto */
    Dizgi opBinary(string işleç, T : dchar[])(T dizgi) const
        if (işleç == "~")
    {
        Dizgi sonuç = this.dup;
        sonuç ~= dizgi;
        return sonuç;
    }

    /**
     * Bu dizginin, belirtilen dizginin sonuna eklenmiş olduğu yeni bir
     * dizgi döndürür
     */
    Dizgi opBinaryRight(string işleç, T : dchar[])(T dizgi) const
        if (işleç == "~")
    {
        auto sonuç = Dizgi(dizgi);
        sonuç.imler_ ~= imler_;
        return sonuç;
    }

    /** ditto */
    Dizgi opBinaryRight(string işleç, T : wchar[])(T dizgi) const
        if (işleç == "~")
    {
        auto sonuç = Dizgi(dtext(dizgi));
        sonuç.imler_ ~= imler_;
        return sonuç;
    }

    /** ditto */
    Dizgi opBinaryRight(string işleç, T : char[])(const T dizgi) const
        if (işleç == "~")
    {
        auto sonuç = Dizgi(dtext(dizgi));
        sonuç.imler_ ~= imler_;
        return sonuç;
    }

    /**
     * Bütün dizgiye erişim sağlayan dilim döndürür
     */
    Dilim!kod opSlice()
    {
        return Dilim!kod(&imler_[0], &imler_[0] + imler_.length);
    }

    /**
     * Dizginin belirtilen aralığına erişim sağlayan dilim döndürür
     */
    Dilim!kod opSlice(size_t baş, size_t son)
    {
        return Dilim!kod(&imler_[baş], &imler_[son]);
    }

    /**
     * Sayaçsız değiştirmeyen foreach
     */
    int opApply(int delegate(const ref İmT) işlem) const
    {
        int sonuç = 0;

        foreach (ref im; imler_) {
            sonuç = işlem(im);

            if (sonuç) {
                break;
            }
        }

        return sonuç;
    }

    /**
     * Sayaçsız foreach
     */
    int opApply(int delegate(ref İmT) işlem)
    {
        int sonuç = 0;

        foreach (ref im; imler_) {
            sonuç = işlem(im);

            if (sonuç) {
                break;
            }
        }

        return sonuç;
    }

    /**
     * Sayaçlı değiştirmeyen foreach
     */
    int opApply(int delegate(ref int i, const ref İmT) işlem) const
    {
        int sonuç = 0;
        int sayaç = 0;

        foreach (ref im; imler_) {
            sonuç = işlem(sayaç, im);

            if (sonuç) {
                break;
            }

            ++sayaç;
        }

        return sonuç;
    }

    /**
     * Sayaçlı foreach
     */
    int opApply(int delegate(ref int i, ref İmT) işlem)
    {
        int sonuç = 0;
        int sayaç = 0;

        foreach (ref im; imler_) {
            sonuç = işlem(sayaç, im);

            if (sonuç) {
                break;
            }

            ++sayaç;
        }

        return sonuç;
    }

    /**
     * Dizginin bütün imleri küçük olanını döndürür
     */
    @property Dizgi küçüğü() const
    {
        Dizgi sonuç = dup();

        foreach (ref im; sonuç.imler_) {
            im = im.küçüğü;
        }

        return sonuç;
    }

    /**
     * Dizginin bütün imleri büyük olanını döndürür
     */
    @property Dizgi büyüğü() const
    {
        Dizgi sonuç = dup();

        foreach (ref im; sonuç.imler_) {
            im = im.büyüğü;
        }

        return sonuç;
    }

    /**
     * Dizginin bütün imleri aksansız olanını döndürür
     */
    @property Dizgi aksansızı() const
    {
        Dizgi sonuç = dup();

        foreach (ref im; sonuç.imler_) {
            im = im.aksansızı;
        }

        return sonuç;
    }

    /**
     * InputRange işlevi empty()
     */
    @property bool empty() const
    {
        return imler_.empty();
    }

    /**
     * InputRange işlevi front()
     */
    @property auto ref front()
    {
        return imler_.front;
    }

    /**
     * InputRange işlevi popFront()
     */
    void popFront()
    {
        imler_.popFront();
    }

    /**
     * ForwardRange işlevi save()
     */
    inout(Dizgi) save() inout
    {
        return this;
    }

    /**
     * BidirectionalRange işlevi back()
     */
    @property auto ref back()
    {
        return imler_.back();
    }

    /**
     * BidirectionalRange işlevi popBack()
     */
    void popBack()
    {
        imler_.popBack();
    }

    /**
     * RandomAccessRange işlevleri opIndex() ve length() zaten tanımlılar
     */

    /**
     * OutputRange işlevi put()
     */
    void put(T)(T eleman)
        if (is (T : İmT))
    {
        imler_ ~= eleman;
    }
    void put(T)(T eleman)
        if (isSomeChar!T)
    {
        imler_ ~= İmT(eleman);
    }
}

/*
 * Bu yardımcı sınıf, Dizgi.opSlice işlevlerinin döndürdüğü değer ile
 * yapılan işlemleri gerçekleştirir
 */
struct Dilim(string kod)
{
private:

    alias İm!kod İmT;

    İmT * baş_;
    İmT * son_;

public:

    @property size_t length() const
    {
        return son_ - baş_;
    }

    @property bool empty() inout
    {
        return baş_ == son_;
    }

    /*
     * Aralıktaki bütün imlere sağdaki tek değeri atar
     */
    void opAssign(T : dchar)(T c)
    {
        foreach (i; 0 .. length) {
            baş_[i] = c;
        }
    }

    /*
     * Aralıktaki imlere sağdakilerin değerlerini atar
     */
    void opAssign(T : Dilim)(T sağdaki)
    in
    {
        assert(length == sağdaki.length);
    }
    body
    {
        foreach (i; 0 .. length) {
            baş_[i] = sağdaki.baş_[i];
        }
    }

    /* ditto */
    void opAssign(T : dchar[])(T sağdaki)
    in
    {
        assert(length == sağdaki.length);
    }
    body
    {
        foreach (i; 0 .. length) {
            baş_[i] = sağdaki[i];
        }
    }

    /* ditto */
    void opAssign(T : char[])(T sağdaki)
    {
        this = dtext(sağdaki);
    }

    @property bool empty() const
    {
        return length == 0;
    }

    @property auto ref front()
    {
        return baş_[0];
    }

    @property İm!kod front() const
    {
        return baş_[0];
    }

    void popFront()
    {
        ++baş_;
    }

    inout(Dilim) save() inout
    {
        return this;
    }

    Dizgi!kod dizgi() const @property
    {
        return Dizgi!kod();
    }

    alias dizgi this;
}

unittest
{
    import std.stdio;

    alias İm!"tur" im_tr;
    alias Dizgi!"tur" dizgi_tr;
    alias Dizgi!"eng" dizgi_en;

    /*
     * Her D karakter türü ile kurulabilmeli
     */

    auto d_ = dizgi_tr();
    auto dc = dizgi_tr("a"c);
    auto dw = dizgi_tr("ğ"w);
    auto dd = dizgi_tr("\U00000e95"d);
    auto di = dizgi_tr([ im_tr('ş') ]);

    assert(d_.length == 0);
    assert(dc.length == 1);
    assert(dw.length == 1);
    assert(dd.length == 1);
    assert(di.length == 1);

    assert(dc[0] == 'a');
    assert(dw[0] == 'ğ');
    assert(dd[0] == '\U00000e95');
    assert(di[0] == im_tr('ş'));

    /*
     * Dizgi türleriyle kurulabilmeli
     */

    char[] değişken_c = "merhaba".dup;
    auto değişken_dc = dizgi_tr(değişken_c);

    wchar[] değişken_w = "merhaba"w.dup;
    auto değişken_dw = dizgi_tr(değişken_w);

    dchar[] değişken_d = "merhaba"d.dup;
    auto değişken_dd = dizgi_tr(değişken_d);

    im_tr[] beklenen = [ im_tr('z'), im_tr('\U00000e95'), im_tr('ş') ];

    // XXX - this(wchar[]) bunun yüzünden derlenmiyor; neden?
    auto dsc = dizgi_tr("z\U00000e95ş");

    // XXX - wchar[] ile kuramıyoruz; neden?
    auto dsw = dizgi_tr(dtext("z\U00000e95ş"w));

    auto dsd = dizgi_tr("z\U00000e95ş"d);
    auto dsi = dizgi_tr(beklenen);
    assert(dsc.length == 3);
    assert(dsw.length == 3);
    assert(dsd.length == 3);
    assert(dsi.length == 3);
    assert(dsc[0] == 'z');
    assert(dsw[1] == '\U00000e95');
    assert(dsd[2] == 'ş');
    assert(dsi[2] == im_tr('ş'));

    assert(dsc == beklenen);
    assert(dsc == "z\U00000e95ş"c);
    assert(dsw == beklenen);
    assert(dsw == "z\U00000e95ş"w);
    assert(dsd == beklenen);
    assert(dsd == "z\U00000e95ş"d);

    assert(dsi == beklenen);
    assert(beklenen == dsc);
    assert(beklenen == dsw);
    assert(beklenen == dsd);
    assert(beklenen == dsi);

    dsc[0] = '\U00000e95';
    beklenen[0] = im_tr('\U00000e95');
    assert(dsc == beklenen);

    dsw[0] = '\U00000e95';
    dsw[1] = 'i';
    beklenen[1] = im_tr('i');
    assert(dsw == beklenen);

    dsd[0] = '\U00000e95';
    dsd[1] = 'i';
    dsd[2] = 'ğ';
    beklenen[2] = im_tr('ğ');
    assert(dsd == beklenen);

    // .dup

    dizgi_tr dsd2 = dsd.dup;
    assert(dsd2 == dsd);
    assert(dsd2 !is dsd);
    assert(dsd2.imler_ == dsd.imler_);
    assert(dsd2.imler_ !is dsd.imler_);

    // .idup

    immutable dizgi_tr dsd_imm = dsd2.idup();
    assert(dsd_imm == dsd2);
    assert(dsd_imm.imler_ !is dsd2.imler_);

    // Ekleme işlemleri

    dsd2 ~= 'c';
    assert(dsd2 == "\U00000e95iğc"c);
    dsd2 ~= 'ı';
    assert(dsd2 == "\U00000e95iğcı"c);
    dsd2 ~= '\U00000e91';
    assert(dsd2 == "\U00000e95iğcı\U00000e91"c);
    dsd2 ~= im_tr('q');
    assert(dsd2 == "\U00000e95iğcı\U00000e91q"c);

    dsd2 ~= "ağ";
    assert(dsd2 == "\U00000e95iğcı\U00000e91qağ"c);
    dsd2 ~= "ağz\U00000e92"w;
    assert(dsd2 == "\U00000e95iğcı\U00000e91qağağz\U00000e92"c);
    dsd2 ~= "oİ"d;
    assert(dsd2 == "\U00000e95iğcı\U00000e91qağağz\U00000e92oİ"c);
    dsd2 ~= [ im_tr('b'), im_tr('â') ];
    assert(dsd2 == "\U00000e95iğcı\U00000e91qağağz\U00000e92oİbâ"c);
    dsd2 ~= dizgi_tr("aı\U00000ea1");
    assert(dsd2 == "\U00000e95iğcı\U00000e91qağağz\U00000e92oİbâaı\U00000ea1"c);

    assert(dsd2.toString()
           == "\U00000e95iğcı\U00000e91qağağz\U00000e92oİbâaı\U00000ea1");

    // Birleştirme işlemleri

    auto eklenen = dizgi_tr("a\U00000ea2ız");

    auto iki_dizgi = eklenen ~ eklenen;
    assert(iki_dizgi == "a\U00000ea2ıza\U00000ea2ız"c);

    char[] cd = "taeÜ".dup;
    cd = (cd ~ eklenen).toString().dup;
    assert(cd == "taeÜa\U00000ea2ız");
    cd = (eklenen ~ cd).toString().dup;
    assert(cd == "a\U00000ea2ıztaeÜa\U00000ea2ız");

    wchar[] wd = "İaş"w.dup;
    wd = to!(wchar[])((wd ~ eklenen).toString());
    assert(wd == "İaşa\U00000ea2ız");
    wd = to!(wchar[])((eklenen ~ wd).toString());
    assert(wd == "a\U00000ea2ızİaşa\U00000ea2ız");

    dchar[] sd = "açz"d.dup;
    sd = to!(dchar[])((sd ~ eklenen).toString());
    assert(sd == "açza\U00000ea2ız");
    sd = to!(dchar[])((eklenen ~ sd).toString());
    assert(sd == "a\U00000ea2ızaçza\U00000ea2ız");

    // Dilim işlemleri

    auto dilimlenen = dizgi_tr("012345678");
    dilimlenen[3 .. 6] = 'a';
    assert(dilimlenen == "012aaa678"c);
    dilimlenen[4 .. 8] = dilimlenen[0 .. 4];
    assert(dilimlenen == "012a012a8"c);
    assert(dilimlenen.length == 9);

    string dilim_beklenen = "üşıöçĞÜŞİ";
    dilimlenen[] = dilim_beklenen;
    assert(dilimlenen == dilim_beklenen);
    assert(dilimlenen.length == 9);

    // Harf işlemleri

    auto kb = dizgi_tr("aÛişABCşIzĞ");
    assert(kb.büyüğü == "AÛİŞABCŞIZĞ"c);
    assert(kb.küçüğü == "aûişabcşızğ"c);
    assert(kb.aksansızı == "aUişABCşIzĞ"c);
    assert((dizgi_en(kb.karakterler)).aksansızı == "aUisABCsIzG"c);
    assert(kb == "aÛişABCşIzĞ"c);

    // Sıra karşılaştırmaları
    {
        assert(dizgi_tr("aeb") < dizgi_tr( "aéb"));
        assert(dizgi_tr("aeb") > dizgi_tr( "aéa"));
        assert(dizgi_tr("aeba") > dizgi_tr( "aéb"));

        auto d0 = dizgi_tr("aBCdef");
        auto d1 = dizgi_tr("aBCDef");
        auto d2 = dizgi_tr("abCdef");
        auto d3 = dizgi_tr("abcdéf");

        // Hiçbirisi eşit olmamalı
        assert(d0 != d1);
        assert(d0 != d2);
        assert(d0 != d3);
        assert(d1 != d2);
        assert(d1 != d3);
        assert(d2 != d3);

        // Sıralamayı küçük/büyük belirlemeli
        assert(d0 < d1);
        assert(d0 > d2);
        assert(d1 > d2);

        /*
         * Temelde aynı imlere sahip olsalar da, küçük/büyük veya aksanlara
         * bakılmadan; sıralamayı uzunluk belirlemeli
         */
        assert(dizgi_tr("AAÂ") < dizgi_tr("aaab"));

        /*
         * Önceki imlerinde küçük/büyük farkları olsa da sıralamayı aksan
         * belirlemeli
         */
        assert(d3 > d0);
        assert(d3 > d1);
        assert(d3 > d2);
    }

    auto güv = dizgi_tr("Güven");
    auto ges = dizgi_tr("Gesin");
    auto gér = dizgi_tr("Gérard");

    assert(gér < ges);
    assert(ges < güv);

    dizgi_tr[] dizgiler = [ güv, ges, gér ];
    dizgiler.sort;
    assert(dizgiler == [ gér, ges, güv ]);

    // Eşleme tablolarında indeks türü olarak kullanılabilmelidir
    {
        /*
         * Not: Testin geçerli olması için bunun içinde tekrarlanan harf
         * bulunmamalıdır
         */
        dstring harfler = `aAâÂbBcCçÇdDeEfFgGğĞhHıIîÎiİjJkKlL`
                          `mMnNoOöÖpPqQrRsSşŞtTuUûÛüÜvVwWxXyYzZ`
                          `~!@#$%^&*(){}',."<>/=?+-_0123456789`;
        int beklenenUzunluk;

        dizgi_tr[dizgi_tr] tablo;
        tablo[dizgi_tr("abcdefg")] = dizgi_tr("abcdefg");
        tablo[dizgi_tr("abcdéfg")] = dizgi_tr("abcdéfg");
        tablo[dizgi_tr("abcdeFg")] = dizgi_tr("abcdeFg");
        tablo[dizgi_tr("abcdefgx")] = dizgi_tr("abcdefgx");

        beklenenUzunluk += 4;

        foreach (harf0; harfler) {
            foreach (harf1; harfler) {
                auto dizgi = dizgi_tr([harf0, harf1]);

                // Hem indeks, hem de değer aynı olsun; aşağıda denetleyeceğiz
                tablo[dizgi] = dizgi.dup;
                ++beklenenUzunluk;
            }
        }

        assert(tablo.length == beklenenUzunluk);

        // Her değere erişilebilmeli
        foreach (değer; tablo.values) {
            dizgi_tr indeks = değer;
            assert(indeks in tablo);
        }

        // Eklediğimiz değerleri okumalıyız
        foreach (indeks; tablo.keys) {
            assert(indeks == tablo[indeks]);
        }
    }

    dstring erişilecek_karakterler = "kâğıt";
    auto erişilecek = dizgi_tr(erişilecek_karakterler);
    int testSayacı = 0;

    // Sayaçsız, değiştirmeyen foreach const ile
    foreach (im; cast(const)erişilecek) {
        assert(im == erişilecek_karakterler[testSayacı]);
        ++testSayacı;
    }

    // Sayaçsız, değiştirmeyen foreach değişebilen ile
    testSayacı = 0;
    testSayacı = 0;
    foreach (im; erişilecek) {
        assert(im == erişilecek_karakterler[testSayacı]);
        ++testSayacı;
    }

    // Sayaçlı, değiştirmeyen foreach const ile
    testSayacı = 0;
    foreach (i, im; cast(const)erişilecek) {
        assert(i == testSayacı);
        assert(im == erişilecek_karakterler[i]);
        ++testSayacı;
    }

    // Sayaçlı, değiştirmeyen foreach değişebilen ile
    testSayacı = 0;
    foreach (i, im; erişilecek) {
        assert(i == testSayacı);
        assert(im == erişilecek_karakterler[i]);
        ++testSayacı;
    }

    // Sayaçsız, değiştiren foreach
    testSayacı = 0;
    foreach (ref im; erişilecek) {
        im = im.büyüğü;
    }
    assert(erişilecek == "KÂĞIT"c);

    // Sayaçlı, değiştiren foreach
    testSayacı = 0;
    foreach (i, ref im; erişilecek) {
        assert(i == testSayacı);
        im = im.aksansızı;
        ++testSayacı;
    }
    assert(erişilecek == "KAĞIT"c);

    // Dilim becerileri
    {
        alias Dilim!"tur" Di;
        static assert(isInputRange!Di);
        static assert(isForwardRange!Di);
        static assert(isBidirectionalRange!Di);
        static assert(isRandomAccessRange!Di);
        static assert(isOutputRange!(Di, İm!"tur"));
        static assert(isOutputRange!(Di, char));
        static assert(isOutputRange!(Di, wchar));
        static assert(isOutputRange!(Di, dchar));
        static assert(!isInfinite!Di);
        static assert(hasLength!Di);
        static assert(hasSlicing!Di);
        static assert(hasAssignableElements!Di);
        static assert(hasSwappableElements!Di);
        static assert(hasMobileElements!Di);
        static assert(hasLvalueElements!Di);
    }

    // Aralık işlevleri
    {
        alias Dizgi!"tur" D;
        static assert(isInputRange!D);
        static assert(isForwardRange!D);
        static assert(isBidirectionalRange!D);
        static assert(isRandomAccessRange!D);
        static assert(isOutputRange!(D, İm!"tur"));
        static assert(isOutputRange!(D, char));
        static assert(isOutputRange!(D, wchar));
        static assert(isOutputRange!(D, dchar));
        static assert(!isInfinite!D);
        static assert(hasLength!D);
        static assert(hasSlicing!D);
        static assert(hasAssignableElements!D);
        static assert(hasSwappableElements!D);
        static assert(hasMobileElements!D);
        static assert(hasLvalueElements!D);

        auto d = D("ğabc");
        auto k = d.front;
        assert(k == 'ğ');
        d.front = im_tr('x');
        assert(d == "xabc");
        d.popFront();
        assert(d == "abc");

        auto d_kopyası = d.save();
        d_kopyası.popFront();
        assert(d_kopyası == "bc");
        assert(d == "abc");

        k = d.back;
        d.back = im_tr('ş');
        assert(d == "abş");
        d.popBack();
        assert(d == "ab");

        d = D("012345");
        d.put(cast(char)'a');
        d.put(cast(wchar)'ç');
        d.put(cast(dchar)'ğ');
        d.put(im_tr('ş'));
        // XXX - İstediğimiz davranış gerçekten bu mu? Yoksa 0123'ün mü üstüne
        // yazılmalıydı?
        assert(d == "012345açğş");
    }
}

class DizgiYazıParçası(string kod) : YazıParçası
{
    Dizgi!kod dizgi_;

    this(Dizgi!kod dizgi)
    {
        dizgi_ = dizgi;
    }

    /** ditto */
    override bool opEquals(Object o) const
    {
        const sağdaki = cast(DizgiYazıParçası)o;
        return (sağdaki !is null) && (dizgi_ == sağdaki.dizgi_);
    }

    bool eşittir(YazıParçası sağdaki) const
    {
        return this.opEquals(cast(Object)sağdaki);
    }

    DizgiYazıParçası dup() const
    {
        return new DizgiYazıParçası(dizgi_.dup);
    }

    @property size_t length() const
    {
        return dizgi_.length;
    }

    @property DizgiYazıParçası küçüğü() const
    {
        return new DizgiYazıParçası(dizgi_.küçüğü);
    }

    @property DizgiYazıParçası büyüğü() const
    {
        return new DizgiYazıParçası(dizgi_.büyüğü);
    }

    @property DizgiYazıParçası aksansızı() const
    {
        return new DizgiYazıParçası(dizgi_.aksansızı);
    }

    @property const(dchar)[] karakterler() const
    {
        return dizgi_.karakterler;
    }

    override string toString() const
    {
        return dizgi_.toString();
    }
}

DizgiYazıParçası!kod dizgi_yazı_parçası(string kod)(Dizgi!kod dizgi)
{
    return new DizgiYazıParçası!kod(dizgi);
}