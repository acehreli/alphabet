/**
 * Bu modül, alfabelerin alt parçalarını oluşturan harf ve harf gruplarını
 * temsil eder.
 *
 * Bu modül daha çok kütüphanenin bir iç modülü olarak
 * düşünülebilir. Kullanıcıların doğrudan bu modülü değil; bundan
 * yararlanan tr.alfabe, tr.im, tr.dizgi, vs. gibi modülleri kullanmaları
 * daha uygun olabilir.
 */

module tr.harfler;

import tr.harf;
import tr.grup;

/**
 * Bu hazır değerler alfabelerin yapı taşlarını oluşturur.
 *
 */

enum boşluk_harfi  = Harf(' ');                         /// ditto

enum a_harfi = Harf('a');
enum A_harfi = Harf('A');                               /// ditto
enum a_aksan = Grup("âàáãäåāăąǎǟǡǻȁȃȧḁạảấầẩẫậắằẳẵặⓐ");  /// ditto
enum A_aksan = Grup("ÂÀÁÃÄÅĀĂĄǍǞǠǺȀȂȦḀẠẢẤẦẨẪẬẮẰẲẴẶⒶ");  /// ditto

enum ä_harfi = Harf('ä');
enum Ä_harfi = Harf('Ä');

enum á_harfi = Harf('á');
enum Á_harfi = Harf('Á');

enum b_harfi = Harf('b');
enum B_harfi = Harf('B');
enum b_aksan = Grup("ḃḅḇƀⓑ");
enum B_aksan = Grup("ḂḄḆƀⒷ");

enum c_harfi = Harf('c');
enum C_harfi = Harf('C');
enum c_aksan = Grup("çćĉċčḉⓒ");
enum C_aksan = Grup("ÇĆĈĊČḈⒸ");

enum ç_harfi = Harf('ç');
enum Ç_harfi = Harf('Ç');
enum ç_aksan = Grup("ḉ");
enum Ç_aksan = Grup("Ḉ");

enum d_harfi = Harf('d');
enum D_harfi = Harf('D');
enum d_aksan = Grup("đďḋḍḏḑḓⓓ");
enum D_aksan = Grup("ĐĎḊḌḎḐḒⒹ");

enum e_harfi = Harf('e');
enum E_harfi = Harf('E');
enum e_aksan = Grup("èéêëēĕėęěȅȇȩḕḗḙḛḝẹẻẽếềểễệⓔ");
enum E_aksan = Grup("ÈÉÊËĒĔĖĘĚȄȆȨḔḖḘḚḜẸẺẼẾỀỂỄỆⒺ");

enum ê_harfi = Harf('ê');
enum Ê_harfi = Harf('Ê');

enum é_harfi = Harf('é');
enum É_harfi = Harf('É');

enum ə_harfi = Harf('ə');
enum Ə_harfi = Harf('Ə');

enum f_harfi = Harf('f');
enum F_harfi = Harf('F');
enum f_aksan = Grup("ḟⓕ");
enum F_aksan = Grup("ḞⒻ");

enum g_harfi = Harf('g');
enum G_harfi = Harf('G');
enum g_aksan = Grup("ĝğġĢģǥǧǵḡⓖ");
enum G_aksan = Grup("ĜĞĠĢģǤǦǴḠⒼ");

enum ƣ_harfi = Harf('ƣ');
enum Ƣ_harfi = Harf('Ƣ');

enum ğ_harfi = Harf('ğ');
enum Ğ_harfi = Harf('Ğ');
enum ğ_aksan = Grup("ĝǧḡ");
enum Ğ_aksan = Grup("ĜǦḠ");

enum h_harfi = Harf('h');
enum H_harfi = Harf('H');
enum h_aksan = Grup("ĥħȟḣḥḧḩḫẖⓗ");
enum H_aksan = Grup("ĤĦȞḢḤḦḨḪẖⒽ");

enum ḧ_harfi = Harf('ḧ');
enum Ḧ_harfi = Harf('Ḧ');

enum i_harfi = Harf('i');
enum I_harfi = Harf('I');
enum ı_harfi = Harf('ı');
enum İ_harfi = Harf('İ');

enum tr_ı_aksan = Grup("îìíïĩīĭǐȉȋḯỉ");
enum tr_I_aksan = Grup("ÎÌÍÏĨĪĬǏȈȊḮỈ");

enum tr_i_aksan = Grup("įɨḭịⓘ");
enum tr_İ_aksan = Grup("ĮƗḬỊⒾ");

// Küçükleri ve büyükleri tam olan i ve I aksanlıları
enum i_sorunsuz_aksan = Grup("ìíîïĩīĭįǐȉȋɨḭḯỉịⓘ");
enum I_sorunsuz_aksan = Grup("ÌÍÎÏĨĪĬĮǏȈȊƗḬḮỈỊⒾ");

/*
 * ı'nın büyüğü, İ'nin de küçüğü bulunmuyor (i ve I'yı kullanamayız; çünkü
 * asıl i ve I ile karışırlar.)
 */
enum i_aksan = ı_harfi ~ (İ_harfi ~ i_sorunsuz_aksan);
enum I_aksan = ı_harfi ~ (İ_harfi ~ I_sorunsuz_aksan);

enum î_harfi = Harf('î');
enum Î_harfi = Harf('Î');

enum į_harfi = Harf('į');
enum Į_harfi = Harf('Į');

enum í_harfi = Harf('í');
enum Í_harfi = Harf('Í');

enum j_harfi = Harf('j');
enum J_harfi = Harf('J');
enum j_aksan = Grup("ĵǰⓙ");
enum J_aksan = Grup("ĴǰⒿ");

enum k_harfi = Harf('k');
enum K_harfi = Harf('K');
enum k_aksan = Grup("ķǩḱḳḵⓚ");
enum K_aksan = Grup("ĶǨḰḲḴⓀ");

enum l_harfi = Harf('l');
enum L_harfi = Harf('L');
enum l_aksan = Grup("ĺļľłḷḹḻḽⓛ");
enum L_aksan = Grup("ĹĻĽŁḶḸḺḼⓁ");

enum m_harfi = Harf('m');
enum M_harfi = Harf('M');
enum m_aksan = Grup("ḿṁṃⓜ");
enum M_aksan = Grup("ḾṀṂⓂ");

enum n_harfi = Harf('n');
enum N_harfi = Harf('N');
enum n_aksan = Grup("ñńņňǹṅṇṉṋⁿⓝ");
enum N_aksan = Grup("ÑŃŅŇǸṄṆṈṊⁿⓃ");

enum ň_harfi = Harf('ň');
enum Ň_harfi = Harf('Ň');

enum ñ_harfi = Harf('ñ');
enum Ñ_harfi = Harf('Ñ');

// Bu harfleri n_aksan ve N_aksan'a ekleyince "range violation" oluyor (?)
enum ŋ_harfi = Harf('ŋ');
enum Ŋ_harfi = Harf('Ŋ');

enum o_harfi = Harf('o');
enum O_harfi = Harf('O');
enum o_aksan = Grup("òóôõöøōŏőơǒǫǭǿȍȏȫȭȯȱṍṏṑṓọỏốồổỗộớờởỡợⓞ");
enum O_aksan = Grup("ÒÓÔÕÖØŌŎŐƠǑǪǬǾȌȎȪȬȮȰṌṎṐṒỌỎỐỒỔỖỘỚỜỞỠỢⓄ");

enum ó_harfi = Harf('ó');
enum Ó_harfi = Harf('Ó');

enum ɵ_harfi = Harf('ɵ');
enum Ɵ_harfi = Harf('Ɵ');

enum ö_harfi = Harf('ö');
enum Ö_harfi = Harf('Ö');
enum ö_aksan = Grup("ȫ");
enum Ö_aksan = Grup("Ȫ");

enum p_harfi = Harf('p');
enum P_harfi = Harf('P');
enum p_aksan = Grup("ṕṗⓟ");
enum P_aksan = Grup("ṔṖⓅ");

enum q_harfi = Harf('q');
enum Q_harfi = Harf('Q');
enum q_aksan = Grup("ⓠ");
enum Q_aksan = Grup("Ⓠ");

enum r_harfi = Harf('r');
enum R_harfi = Harf('R');
enum r_aksan = Grup("ŕŗřȑȓṙṛṝṟⓡ");
enum R_aksan = Grup("ŔŖŘȐȒṘṚṜṞⓇ");

enum s_harfi = Harf('s');
enum S_harfi = Harf('S');

enum s_aksan = Grup("śŝşšșṡṣṥṧṩßẛſⓢ");
enum S_aksan = Grup("ŚŜŞŠȘṠṢṤṦṨßẛſⓈ");

enum ş_harfi = Harf('ş');
enum Ş_harfi = Harf('Ş');
enum ş_aksan = Grup("șṣṩ");
enum Ş_aksan = Grup("ȘṢṨ");

enum t_harfi = Harf('t');
enum T_harfi = Harf('T');
enum t_aksan = Grup("ţťŧțṫṭṯṱẗⓣ");
enum T_aksan = Grup("ŢŤŦȚṪṬṮṰẗⓉ");

enum ţ_harfi = Harf('ţ');
enum Ţ_harfi = Harf('Ţ');

enum u_harfi = Harf('u');
enum U_harfi = Harf('U');
enum u_aksan = Grup("ûùúüũūŭůűųưǔǖǘǚǜȕȗṳṵṷṹṻụủứừửữựⓤ");
enum U_aksan = Grup("ÛÙÚÜŨŪŬŮŰŲƯǓǕǗǙǛȔȖṲṴṶṸṺỤỦỨỪỬỮỰⓊ");

enum û_harfi = Harf('û');
enum Û_harfi = Harf('Û');

enum ú_harfi = Harf('ú');
enum Ú_harfi = Harf('Ú');

enum ü_harfi = Harf('ü');
enum Ü_harfi = Harf('Ü');
enum ü_aksan = Grup("ǖǘǚǜ");
enum Ü_aksan = Grup("ǕǗǙǛ");

enum v_harfi = Harf('v');
enum V_harfi = Harf('V');
enum v_aksan = Grup("ṽṿⓥ");
enum V_aksan = Grup("ṼṾⓋ");

enum w_harfi = Harf('w');
enum W_harfi = Harf('W');
enum w_aksan = Grup("ŵẁẃẅẇẉẘⓦ");
enum W_aksan = Grup("ŴẀẂẄẆẈẘⓌ");

enum x_harfi = Harf('x');
enum X_harfi = Harf('X');
enum x_aksan = Grup("ẋẍⓧ");
enum X_aksan = Grup("ẊẌⓍ");

enum ẍ_harfi = Harf('ẍ');
enum Ẍ_harfi = Harf('Ẍ');

enum y_harfi = Harf('y');
enum Y_harfi = Harf('Y');
enum y_aksan = Grup("ýŷÿȳẏỳỵỷỹẙⓨ");
enum Y_aksan = Grup("ÝŶŸȲẎỲỴỶỸẙⓎ");

enum ý_harfi = Harf('ý');
enum Ý_harfi = Harf('Ý');

enum z_harfi = Harf('z');
enum Z_harfi = Harf('Z');
enum z_aksan = Grup("źżžƶẑẓẕⓩ");
enum Z_aksan = Grup("ŹŻŽƵẐẒẔⓏ");

enum ž_harfi = Harf('ž');
enum Ž_harfi = Harf('Ž');

enum ƶ_harfi = Harf('ƶ');
enum Ƶ_harfi = Harf('Ƶ');

enum ь_harfi = Harf('ь');
enum Ь_harfi = Harf('Ь');
