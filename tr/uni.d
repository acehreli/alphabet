/**
 * Copyright: Açık kod
 * Version: 0.1
 */

module tr.uni;

private import std.uni;

/**
 * Unicode karakterin küçüğünü döndürür
 *
 * Phobos kütüphanesindeki eşdeğeri toLower'dan farklı olarak I'nın
 * dönüşümünü Türkçe'ye uygun olarak gerçekleştirir: I -> ı
 *
 * Params:
 *     karakter = Küçüğüne çevrilecek karakter
 *
 * Returns: Verilen karakterin küçüğü
 *
 * Examples:
 * ---
 * toLower_tr('I'); // 'ı' döndürür
 * ---
 *
 */
dchar toLower_tr(in dchar karakter)
{
    return (karakter == 'I') ? 'ı' : toLower(karakter);
}
unittest
{
     char Ic = 'I';
    wchar Iw = 'I';
    dchar Id = 'I';

    assert(toLower_tr('I') == 'ı');
    assert(toLower_tr(Ic) == 'ı');
    assert(toLower_tr(Iw) == 'ı');
    assert(toLower_tr(Id) == 'ı');

    assert(toLower_tr('E') == 'e');
}

/**
 * Unicode karakterin büyüğünü döndürür
 *
 * Phobos kütüphanesindeki eşdeğeri toUpper'dan farklı olarak i'nin
 * dönüşümünü Türkçe'ye uygun olarak gerçekleştirir: i -> İ
 *
 * Params:
 *     karakter = Büyüğe çevrilecek karakter
 *
 * Returns: Verilen karakterin büyüğü
 *
 * Examples:
 * ---
 * toUpper_tr('i'); // 'İ' döndürür
 * ---
 *
 */
dchar toUpper_tr(in dchar karakter)
{
    return (karakter == 'i') ? 'İ' : toUpper(karakter);
}
unittest
{
     char ic = 'i';
    wchar iw = 'i';
    dchar id = 'i';

    assert(toUpper_tr('i') == 'İ');
    assert(toUpper_tr(ic) == 'İ');
    assert(toUpper_tr(iw) == 'İ');
    assert(toUpper_tr(id) == 'İ');

    assert(toUpper_tr('e') == 'E');
}
