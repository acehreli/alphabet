/*
 * Ddili.org trileri kütüphane projesi
 * bilgi için : ddili.org , ddili.org/wiki , ddili.org/forum
 * Sürüm : 0.001
 */

module tr.string;

import std.utf;
import tr.alfabe;
import tr.uni;
import std.uni;
import std.conv;
import tr.im;
import tr.dizgi;

debug import std.stdio;

/**
 * Dizginin büyüğünü döndürür
 *
 * Phobos kütüphanesindeki eşdeğeri toUpper'dan farklı olarak i'nin
 * dönüşümünü Türkçe'ye uygun olarak gerçekleştirir: ı -> I i -> İ
 *
 * Params:
 *     dizgi = Büyüğüne çevrilecek dizgi
 *
 * Returns: Verilen dizginin büyüğü
 *
 * Examples:
 * ---
 * toUpper_tr("iıöüçğ"); // "İIÖÜÇĞ" döndürür
 * ---
 *
 */
dstring toUpper_tr(in dchar[] dizgi)
{
    return toUpper!"tur"(dizgi);
}

dstring toUpper(string kod)(in dchar[] dizgi)
{
    dchar[] çıkış;
    çıkış.length = dizgi.length;

    foreach(int i, dchar küçükHarf; dizgi) {
        çıkış[i] = alfabe!kod.büyüğü(küçükHarf);
    }

    return çıkış.idup;
}
unittest
{
    assert(toUpper_tr("iıİI") == "İIİI");
    assert(toUpper_tr("ğşç") == "ĞŞÇ");
    assert(toUpper_tr("öü") == "ÖÜ");
}

/**
 * Dizginin küçüğünü döndürür
 *
 * Phobos kütüphanesindeki eşdeğeri toLower'dan farklı olarak I'nın
 * dönüşümünü Türkçe'ye uygun olarak gerçekleştirir: I -> ı İ -> i
 *
 * Params:
 *     dizgi = Küçüğüne çevrilecek dizgi
 *
 * Returns: Verilen dizginin küçüğü
 *
 * Examples:
 * ---
 * toLower_tr("İIÖÜÇĞ"); // "iıöüçğ" döndürür
 * ---
 *
 */
dstring toLower_tr(in dchar[] dizgi)
{
    return toLower!"tur"(dizgi);
}

dstring toLower(string kod)(in dchar[] dizgi)
{
    dchar[] çıkış;
    çıkış.length = dizgi.length;

    foreach(int i, dchar büyükHarf; dizgi) {
        çıkış[i] = alfabe!kod.küçüğü(büyükHarf);
    }

    return çıkış.idup;
}
unittest
{
    assert(toLower_tr("ALİ") == "ali");
    assert(toLower_tr("iıİI") == "iıiı");
    assert(toLower_tr("ĞŞÇ") == "ğşç");
    assert(toLower_tr("ÖÜ") == "öü");
}

/**
 * Dizginin baş harfi büyük ve diğer harfleri küçük olanını döndürür
 *
 * Phobos kütüphanesindeki eşdeğeri capitalize'den farklı olarak I'nın ve
 * i'nin dönüşümlerini Türkçe'ye uygun olarak gerçekleştirir: I -> ı i -> İ
 *
 * Params:
 *     dizgi = Baş harfi büyük ve diğer harfleri küçük olanına çevrilecek dizgi
 *
 * Returns: Verilen dizginin baş harfi büyük ve diğer harfleri küçük olanı
 *
 * Examples:
 * ---
 * capitalize_tr("İNSANLAR"); // "İnsanlar" döndürür
 * ---
 *
 */
dstring capitalize_tr(in dchar[] dizgi)
{
    dchar[] dizgiDchar = dizgi.dup;
    dizgiDchar[0]    = tr.uni.toUpper_tr(dizgi[0]);
    dizgiDchar[1..$] = toLower_tr(dizgi[1..$]);
    return dizgiDchar.idup;
}
unittest
{
    assert(capitalize_tr("ALİ") == "Ali");
    assert(capitalize_tr("İNSANLAR") == "İnsanlar");
    assert(capitalize_tr("INaNç")=="Inanç");
    //ınanç öztürkçe bir kelimedir. Sonra inanç olmuştur.

    assert(capitalize_tr("ii") == "İi");
    assert(capitalize_tr("II") == "Iı");
}

/**
 *
 * Dizginin kelimelerinin baş harfi büyük olanlarını döndürür
 *
 * Kelimelerin baş harfi büyük ve diğer harfleri küçük olanını, baştaki ve
 * sondaki boşlukları silinmiş, ve aralardaki boşlukları tek boşluk
 * karakteri ile değiştirilmiş olanını döndürür. Phobos kütüphanesindeki
 * eşdeğeri capwords'den farklı olarak I'nın ve i'nin dönüşümlerini
 * Türkçe'ye uygun olarak gerçekleştirir: I -> ı i -> İ
 *
 * Params:
 *     dizgi = Baş harfi büyük ve diğer harfleri küçük olanına çevrilecek dizgi
 *
 * Returns: Verilen dizginin baş harfi büyük ve diğer harfleri küçük olanı
 *
 * Examples:
 * ---
 * capwords_tr("IN  a  Nç  "); // "In A Nç" döndürür
 * ---
 *
 */
dstring capwords_tr(in dchar[] dizgi)
{
    dchar[] r;
    bool inword = false;
    size_t istart = 0;
    size_t i;

    for (i = 0; i < dizgi.length; i++) {
        switch (dizgi[i]) {
        case ' ':
        case '\t':
        case '\f':
        case '\r':
        case '\n':
        case '\v':
                if (inword) {
                    r ~= capitalize_tr(dizgi[istart .. i]);
                    inword = false;
                }
                break;

            default:
                if (!inword)        {
                    if (r.length){
                    r ~= ' ';
                    }
                istart = i;
                inword = true;
                }
                break;
        }
    }
    if (inword) {
        r ~= capitalize_tr(dizgi[istart .. i]);
    }

    return r.idup;
}
unittest
{
    assert(capwords_tr("A  Lİ") == "A Li");
    assert(capwords_tr("İ nS  ANLAR") == "İ Ns Anlar");
    assert(capwords_tr("IN  a  Nç  ")=="In A Nç");
    //ınanç öztürkçe bir kelimedir. Sonra inanç olmuştur.

    assert(capwords_tr("   ii") == "İi");
    assert(capwords_tr("  ıI") == "Iı");
}

/**
 * Dizgiyi büyüğüne çevirir
 *
 * Phobos kütüphanesindeki eşdeğeri toUpperInPlace'den farklı olarak i'nin
 * dönüşümünü Türkçe'ye uygun olarak gerçekleştirir: ı -> I i -> İ
 *
 * Params:
 *     dizgi = Büyüğüne çevrilecek dizgi
 *
 * Examples:
 * ---
 * toUpperInPlace_tr("iıöüçğ"); // "İIÖÜÇĞ" çevirir.
 * ---
 *
 */
void toUpperInPlace_tr(T)(ref T[] dizgi)
{
    toUpperInPlace!"tur"(dizgi);
}

void toUpperInPlace(string kod, T)(ref T[] dizgi)
{
    auto sonuç = Dizgi!kod(dizgi);
    sonuç = sonuç.büyüğü;
    dizgi = to!(T[])(sonuç.toString());
}

unittest
{
    dchar[] d = "aıiğşöz"d.dup;
    toUpperInPlace_tr(d);
    assert(d == "AIİĞŞÖZ");

    wchar[] w = "ğıaibBcıĞizİŞIıi"w.dup;
    toUpperInPlace_tr(w);
    assert(w == "ĞIAİBBCIĞİZİŞIIİ");

    char[] c = "aiığz"c.dup;
    toUpperInPlace_tr(c);
    assert(c == "AİIĞZ");
}

/**
 * Dizgiyi küçüğüne çevirir
 *
 * Phobos kütüphanesindeki eşdeğeri toLowerInPlace'den farklı olarak I'nın
 * dönüşümünü Türkçe'ye uygun olarak gerçekleştirir: I -> ı İ -> i
 *
 * Params:
 *     dizgi = Küçüğüne çevrilecek dizgi
 *
 * Examples:
 * ---
 * toLowerInPlace_tr("iıöüçğ"); // "İIÖÜÇĞ" çevirir.
 * ---
 *
 */
void toLowerInPlace_tr(T)(ref T[] dizgi)
{
    return toLowerInPlace!"tur"(dizgi);
}

void toLowerInPlace(string kod, T)(ref T[] dizgi)
{
    auto sonuç = Dizgi!kod(dizgi);
    sonuç = sonuç.küçüğü;
    dizgi = to!(T[])(sonuç.toString());
}

unittest
{
    dchar[] d = "AIİĞŞÖZ"d.dup;
    toLowerInPlace_tr(d);
    assert(d == "aıiğşöz");

    wchar[] w = "ĞIAİBbCIĞİZİşIıİ"w.dup;
    toLowerInPlace_tr(w);
    assert(w == "ğıaibbcığizişııi");

    char[] c = "AİIĞZ"c.dup;
    toLowerInPlace_tr(c);
    assert(c == "aiığz");
}

/*
 * Sıralanmalarının düzeltilmesi gereken harflerin sıra numaraları
 */
private static immutable double[dchar] cmpAbc;

static this()
{
    cmpAbc =
    [
        'ç' : 'c' + 0x0.8p0,
        'ğ' : 'g' + 0x0.8p0,
        'ı' : 'h' + 0x0.8p0,
        'ö' : 'o' + 0x0.8p0,
        'ş' : 's' + 0x0.8p0,
        'ü' : 'u' + 0x0.8p0,
        'Ç' : 'C' + 0x0.8p0,
        'Ğ' : 'G' + 0x0.8p0,
        'İ' : 'I' + 0x0.8p0,
        'Ö' : 'O' + 0x0.8p0,
        'Ş' : 'S' + 0x0.8p0,
        'Ü' : 'U' + 0x0.8p0
    ];
}

/**
 * Harfin sıra numarasını döndürür
 *
 * Params:
 *     karakter = Sıra numarası döndürülecek karakter
 *
 * Returns: Karakterin karşılaştırmada kullanılacak sıra numarası
 *
 * Examples:
 * ---
 * sıraNumarası_tr('ğ'); // 'g' ile 'h'nin sıra numaraları arasında bir
 *                       // değer döndürür
 * ---
 *
 */
double sıraNumarası_tr(T = dchar)(in T karakter)
{
    auto numara = karakter in cmpAbc;
    return (numara) ? *numara : karakter;
}
unittest
{
    assert(sıraNumarası_tr('ğ') < sıraNumarası_tr('h'));
    assert(sıraNumarası_tr('g') < sıraNumarası_tr('ğ'));
    assert(sıraNumarası_tr('ı') < sıraNumarası_tr('i'));
    assert(sıraNumarası_tr('h') < sıraNumarası_tr('ı'));
}

/*
 * Dizginin belirtilen harfinin sıra numarasını döndürür
 *
 * Harfi UTF dizisinden ayıklamak için std.utf.decode'u kullandığı için
 * bunun arayüzü de onunki gibi. Bir sonraki harfin indeksini 'indeks'
 * parametresinde bildirir.
 *
 * Params:
 *     dizgi  = Karakterinin sıra numarası döndürülecek dizgi
 *     indeks = Dizgideki karakterin indeksi; bir sonraki harfi gösterecek
 *              şekilde ilerletilir
 *
 * Returns: Karakterin karşılaştırmada kullanılacak sıra numarası
 *
 */
private double sıraNumarası_tr(T)(in T dizgi, ref size_t indeks)
{
    return sıraNumarası_tr(decode(dizgi, indeks));
}
unittest
{
    void testDoğruSıra(in dchar önceki, in dchar sonraki)
    {
        dchar[] öncekiDizgi;
        öncekiDizgi ~= önceki;

        dchar[] sonrakiDizgi;
        sonrakiDizgi ~= sonraki;

        size_t i1;
        size_t i2;

        double sıra1 = sıraNumarası_tr(öncekiDizgi, i1);
        double sıra2 = sıraNumarası_tr(sonrakiDizgi, i2);

        debug writefln("%s: %5.2f  %s: %5.2f", önceki, sıra1, sonraki, sıra2);

        assert(sıra1 < sıra2);
    }

    void testDoğruSıra_dizgi(in dstring sıralıDizgi)
    {
        for (int i = 0; i <= sıralıDizgi.length - 2; ++i) {
            testDoğruSıra(sıralıDizgi[i], sıralıDizgi[i + 1]);
        }
    }

    testDoğruSıra_dizgi("abcçdefgğhıijklmnoöpqrsştuüvwxyz");
    testDoğruSıra_dizgi("ABCÇDEFGĞHIİJKLMNOÖPQRSŞTUÜVWXYZ");
}

/**
 * İki dizgiyi alfabetik olarak karşılaştırır
 *
 * Phobos kütüphanesindeki eşdeğeri cmp'tan farklı olarak Türk
 * alfabesindeki sırayı kullanır
 *
 * Params:
 *     birinci = Soldaki dizgi
 *     ikinci  = Sağdaki dizgi
 *
 * Returns: Soldaki dizgi önceyse eksi bir değer, sağdaki dizgi önceyse
 *          artı bir değer, eşitlerse sıfır değeri
 *
 * Examples:
 * ---
 * assert(cmp_tr("ıa"d, "ia"d) < 0);
 * assert(cmp_tr("ib"w, "ıb"w) > 0);
 * assert(cmp_tr("ğş"d, "ğş"d) == 0);
 * ---
 */
int cmp_tr(T)(in T birinci, in T ikinci)
{
    size_t i1, i2;

    while (true)
    {
        if (i1 == birinci.length) return cast(int)i2 - cast(int)ikinci.length;
        if (i2 == ikinci.length) return cast(int)birinci.length - cast(int)i1;

        immutable double sıra1 = sıraNumarası_tr(birinci, i1);
        immutable double sıra2 = sıraNumarası_tr(ikinci, i2);

        if (sıra1 < sıra2) return -1;
        if (sıra1 > sıra2) return 1;
    }
}
unittest
{
    assert(cmp_tr("aa", "aaa") < 0);
    assert(cmp_tr("aa"w, "aaa"w) < 0);
    assert(cmp_tr("aaa", "aa") > 0);
    assert(cmp_tr("deneme", "debeme") > 0);
    assert(cmp_tr("çalışkan"d, "çalişkan"d) < 0);
    assert(cmp_tr("çalışkan"d.dup, "çalişkan"d.dup) < 0);
    assert(cmp_tr("çalışkan"w, "çalişkan"w) < 0);
    assert(cmp_tr("çalışkan"w.dup, "çalişkan"w.dup) < 0);
    assert(cmp_tr("TürkçeyeUygun"d.dup, "TürkçeyeUygun"d.dup) == 0);

    assert(cmp_tr("ıa"d, "ia"d) < 0);
    assert(cmp_tr("ib"w, "ıb"w) > 0);
    assert(cmp_tr("ğş"d, "ğş"d) == 0);

    assert(cmp_tr("Türk çeyeUygun"d.dup, "TürkçeyeUygun"d.dup) < 0);
    assert(cmp_tr("çç"d.dup,"çççç"d.dup) < 0);
    assert(cmp_tr("çççç"d.dup, "çç"d.dup) > 0);
    assert(cmp_tr("Çç"d.dup,"çç"d.dup) < 0);
    assert(cmp_tr("çç"d.dup, "Çç"d.dup) > 0);

    assert(cmp_tr("ı", "i") < 0);
}

/**
 * İki dizgiyi alfabetik olarak karşılaştırır
 *
 * Phobos kütüphanesindeki eşdeğeri icmp'tan farklı olarak Türk
 * alfabesindeki sırayı kullanır
 *
 * Params:
 *     birinci = Soldaki dizgi
 *     ikinci  = Sağdaki dizgi
 *
 * Returns: Soldaki dizgi önceyse eksi bir değer, sağdaki dizgi önceyse
 *          artı bir değer, eşitlerse sıfır değeri
 *
 * Examples:
 * ---
 * assert(cmp_tr("ıa"d, "ia"d) < 0);
 * assert(cmp_tr("ib"w, "ıb"w) > 0);
 * assert(cmp_tr("ğş"d, "ğş"d) == 0);
 * ---
 */
int icmp_tr(T)(in T birinci, in T ikinci)
{
    auto b = birinci.dup;
    auto i = ikinci.dup;

    toLowerInPlace_tr(b);
    toLowerInPlace_tr(i);

    return cmp_tr(b, i);
}
unittest
{
    assert(icmp_tr("aa", "aaa") < 0);
    assert(icmp_tr("aa"w, "aaa"w) < 0);
    assert(icmp_tr("aaa", "aa") > 0);
    assert(icmp_tr("deneme", "debeme") > 0);
    assert(icmp_tr("çalışkan"d, "çalişkan"d) < 0);
    assert(icmp_tr("çalışkan"d.dup, "çalişkan"d.dup) < 0);
    assert(icmp_tr("çalışkan"w, "çalişkan"w) < 0);
    assert(icmp_tr("çalışkan"w.dup, "çalişkan"w.dup) < 0);
    assert(icmp_tr("TürkçeyeUygun"d.dup, "TürkçeyeUygun"d.dup) == 0);

    assert(icmp_tr("ıa"d, "ia"d) < 0);
    assert(icmp_tr("ib"w, "ıb"w) > 0);
    assert(icmp_tr("ğş"d, "ğş"d) == 0);

    assert(icmp_tr("Türk çeyeUygun"d.dup, "TürkçeyeUygun"d.dup) < 0);
    assert(icmp_tr("çç"d.dup,"çççç"d.dup) < 0);
    assert(icmp_tr("çççç"d.dup, "çç"d.dup) > 0);
    assert(icmp_tr("Çç"d.dup,"çç"d.dup) == 0);
    assert(icmp_tr("çç"d.dup, "Çç"d.dup) == 0);

    assert(icmp_tr("I", "i") < 0);
    assert(icmp_tr("ı", "İ") < 0);
}
